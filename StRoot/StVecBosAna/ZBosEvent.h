#ifndef ZBosEvent_h
#define ZBosEvent_h

#include "TLorentzVector.h"
#include "TVector3.h"

#include "VecBosEvent.h"
#include "VecBosTrack.h"


/**
 * This class extends the data structure and functionality of #VecBosEvent which is saved in the
 * output ROOT tree. The events described by this class are assumed to pass the Z boson
 * requirements.
 */
class ZBosEvent : public VecBosEvent
{

public:

   ZBosEvent(float minTrackPt=0, int RhicRunId=11);
   ~ZBosEvent();

   VecBosTrack& GetElectronTrack() const;
   Double_t       EOverP_Cand1;
   Double_t       EOverP_ElectronCand;
   Double_t       EOverP_PositronCand;
   TLorentzVector mP4ZBoson;
   TLorentzVector mP4ZBosonSameCharge;
   TVector3           GetCandidate1_P3() const;
   TVector3           GetCandidate2_P3() const;
   TVector3           GetCandidate2_sameCharge_pos_P3() const;
   TVector3           GetCandidate2_sameCharge_neg_P3() const;
   TLorentzVector     GetCandidate1_P4() const;
   TLorentzVector     GetCandidate2_P4() const;
   TLorentzVector     GetElectronCand_P4() const;
   TLorentzVector     GetPositronCand_P4() const;
   TVector3           GetVecBosonP3() const;
   TLorentzVector     GetVecBosonP4() const;
   TLorentzVector     GetVecBosonP4_sameCharge() const;
   

   TVector3         mP3TrackRecoilTpc_Test;           ///< Vector sum of primary tracks, i.e. tracks coming from the primary vertex, except the lepton candidate track   
     //TVector3         mP3TrackRecoilTow_Test;           ///< Vector sum of towers associated with primary tracks
   TVector3         mP3TrackRecoilNeutrals_Test;      ///< Vector sum of all towers without matching track
   TVector3         mP3TrackRecoilTpcNeutrals_Test;     ///< Vector sum of primary tracks and all towers


   //TVector3       GetTrackRecoil_Test()            const { return mP3TrackRecoilTow_Test; }
   //TVector3       GetTrackRecoilNeutrals_Test()    const { return mP3TrackRecoilNeutrals; }
   //TVector3       GetTrackRecoilTpcNeutrals_Test() const { return mP3TrackRecoilTpcNeutrals; }
   
   virtual void Process(int McType);
   virtual void ProcessPersistent();
   virtual void ProcessMC(int McType);
   virtual void Clear(const Option_t* opt="");
   virtual void Print(const Option_t* opt="") const;
   bool         PassedCutZBos(float minElePt=sMinZEleCandPtLight) const;
   bool         PassedCutZSameChargePos(float minElePt=sMinZEleCandPtLight) const;
   bool         PassedCutZSameChargePosZMassExt(float minElePt=sMinZEleCandPtLight) const;//Xiaoxuan
   bool         PassedCutZSameChargeNeg(float minElePt=sMinZEleCandPtLight) const;
   bool         PassedCutZSameChargeNegZMassExt(float minElePt=sMinZEleCandPtLight) const;//Xiaoxuan
   bool         PassedCutZMass(float minElePt=sMinZEleCandPtLight) const;
   bool         PassedCutZMassExt(float minElePt=sMinZEleCandPtLight) const;
   bool         PassedCutZMass80(float minElePt=sMinZEleCandPtLight) const;
   bool         PassedCutZRecoilTest(float minElePt=sMinZEleCandPtLight) const;

   //static const float sMinElectronPtLight; //!
   //static const float sMinElectronPtHard;  //!
   //static const float sMinNeutrinoPt;      //!
   static const float sMinZEleCandPtLight; //! S.Fazio 30Sep2013
   static const float sMinZEleCandPtHard;  //!
   static const float sMinRecoilPt;        //!
   static const float sMaxRecoilPt;        //!
   static const float sMinZMass;           //! 
   static const float sMaxZMass;           //! 

protected:

   void      CalcZBosP4();
   //void      CalcRecoilFromTracksTest(VecBosTrack cand1, VecBosTrack cand2);

   float            mZBosMass;   
   TVector3         mCand1P3; 
   TVector3         mCand2P3;
   TVector3         mCand2P3_sameCharge_positive;  // for test
   TVector3         mCand2P3_sameCharge_negative;  // for test
   TLorentzVector   mP4Cand1; 
   TLorentzVector   mP4Cand2;  
   TLorentzVector   mP4Cand2_sameCharge_positive;  // for test   
   TLorentzVector   mP4Cand2_sameCharge_negative;  // for test 
   TLorentzVector   mP4ElectronCand; 
   TLorentzVector   mP4PositronCand;
   

   ClassDef(ZBosEvent, 2);
};

#endif
