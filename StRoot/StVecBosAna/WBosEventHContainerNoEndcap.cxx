#include "WBosEventHContainerNoEndcap.h"

#include "TF1.h"
#include "TF2.h"

#include "TrackHContainer.h"
#include "JetHContainer.h"
#include "WBosEvent.h"


ClassImp(WBosEventHContainerNoEndcap)

using namespace std;


/** Default constructor. */
WBosEventHContainerNoEndcap::WBosEventHContainerNoEndcap() : PlotHelper()
{
   BookHists();
}


WBosEventHContainerNoEndcap::WBosEventHContainerNoEndcap(TDirectory *dir) : PlotHelper(dir)
{
   BookHists();
}


/** */
void WBosEventHContainerNoEndcap::BookHists()
{
   string shName;
   TH1*   hist;

   // Define a pseudo-rapidity bin for the electron. This is used in the cross-section ratios analisys.
   Double_t ElecEtaBins_xsecRatio[9]  = {-1.1, -0.8, -0.5, -0.25, 0.0, 0.25, 0.5, 0.8, 1.1};

   fDir->cd();

   o["hElectronPt_NoEndcap"]       = hist = new TH1D("hElectronPt_NoEndcap", "; Electron P_{T}; Events", 22, 16, 60); // S.F. APR08 2020 
   o["hElectronEta_NoEndcap"] = hist = new TH1D("hElectronEta_NoEndcap", "; Electron #eta; Events", 20, -2, 2);
   o["hElectronEta_NoEndcap_8bins"]= hist = new TH1D("hElectronEta_NoEndcap_8bins", "; Electron #eta; Events", 8, ElecEtaBins_xsecRatio);
   o["hWBosonPt_NoEndcap"]         = hist = new TH1D("hWBosonPt_NoEndcap", "; W Boson P_{T}; Events", 20, 0, 20);
   o["hWBosonPt_NoEndcap_zoomin"]  = hist = new TH1D("hWBosonPt_NoEndcap_zoomin", "   ; W Boson P_{T}; Events", 20, 0, 10);
   o["hWBosonRapidity_NoEndcap"] = hist = new TH1D("hWBosonRapidity_NoEndcap", "; W Boson Rapidity; Events", 20, -1.5, 1.5);

   // Used for the cross-section ratio analysis
   o["hWBosonRapidity_NoEndcap_5bins"]  = hist = new TH1D("hWBosonRapidity_NoEndcap_5bins", "; W Boson Rapidity; Events", 5, -0.6, 0.6);
   o["hWBosonRapidity_NoEndcap_4bins"]  = hist = new TH1D("hWBosonRapidity_NoEndcap_4bins", "; W Boson Rapidity; Events", 4, -0.6, 0.6);
   Double_t yres = 0.22;
   o["hWBosonRapidity_NoEndcap_5bins_PR"]  = hist = new TH1D("hWBosonRapidity_NoEndcap_5bins_PR", "; W Boson Rapidity; Events", 5, -0.6+yres, 0.6+yres);
   o["hWBosonRapidity_NoEndcap_5bins_MR"]  = hist = new TH1D("hWBosonRapidity_NoEndcap_5bins_MR", "; W Boson Rapidity; Events", 5, -0.6-yres, 0.6-yres);
   o["hWBosonRapidity_NoEndcap_4bins_PR"]  = hist = new TH1D("hWBosonRapidity_NoEndcap_4bins_PR", "; W Boson Rapidity; Events", 4, -0.6+yres, 0.6+yres);
   o["hWBosonRapidity_NoEndcap_4bins_MR"]  = hist = new TH1D("hWBosonRapidity_NoEndcap_4bins_MR", "; W Boson Rapidity; Events", 4, -0.6-yres, 0.6-yres);
   //------------------------------------------

   d["track_candidate"] = new TrackHContainer(new TDirectoryFile("track_candidate", "track_candidate", "", fDir));
   d["jets_recoil"] = new JetHContainer(new TDirectoryFile("jets_recoil", "jets_recoil", "", fDir));
}


/** */
void WBosEventHContainerNoEndcap::Fill(ProtoEvent &ev)
{
   WBosEvent& event = (WBosEvent&) ev;

   ((TH1*) o["hElectronPt_NoEndcap"])->Fill (event.GetElectronP3_NoEndcap().Pt());
   ((TH1*) o["hElectronEta_NoEndcap"])->Fill(event.GetElectronP3_NoEndcap().Eta());
   ((TH1*) o["hElectronEta_NoEndcap_8bins"])->Fill(event.GetElectronP3_NoEndcap().Eta());
   ((TH1*) o["hWBosonPt_NoEndcap"])       -> Fill (event.GetVecBosonP3_NoEndcap().Pt());
   ((TH1*) o["hWBosonPt_NoEndcap_zoomin"])-> Fill (event.GetVecBosonP3_NoEndcap().Pt());

   ((TH1*) o["hWBosonRapidity_NoEndcap"]) -> Fill(event.GetVecBosonP4_NoEndcap().Rapidity());
   ((TH1*) o["hWBosonRapidity_NoEndcap_5bins"])    -> Fill(event.GetVecBosonP4_NoEndcap().Rapidity());
   ((TH1*) o["hWBosonRapidity_NoEndcap_5bins_PR"]) -> Fill(event.GetVecBosonP4_NoEndcap().Rapidity());
   ((TH1*) o["hWBosonRapidity_NoEndcap_5bins_MR"]) -> Fill(event.GetVecBosonP4_NoEndcap().Rapidity());
   ((TH1*) o["hWBosonRapidity_NoEndcap_4bins"])    -> Fill(event.GetVecBosonP4_NoEndcap().Rapidity());
   ((TH1*) o["hWBosonRapidity_NoEndcap_4bins_PR"]) -> Fill(event.GetVecBosonP4_NoEndcap().Rapidity());
   ((TH1*) o["hWBosonRapidity_NoEndcap_4bins_MR"]) -> Fill(event.GetVecBosonP4_NoEndcap().Rapidity());

   ((TrackHContainer*) d["track_candidate"])->Fill(event.GetElectronTrackNoEndcap());
   ((JetHContainer*) d["jets_recoil"])->Fill(event.mJetsRecoil_NoEndcap);
}


/** */
void WBosEventHContainerNoEndcap::FillDerived()
{
   Info("FillDerived()", "Called");
}


/** */
void WBosEventHContainerNoEndcap::PostFill()
{
   Info("PostFill", "Called");
}
