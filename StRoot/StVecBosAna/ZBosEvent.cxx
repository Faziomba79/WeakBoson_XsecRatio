
#include "ZBosEvent.h"
#include "ZBosMcEvent.h"

#include "utils/utils.h"

ClassImp(ZBosEvent)

using namespace std;


const float ZBosEvent::sMinZEleCandPtLight = 15;
const float ZBosEvent::sMinZEleCandPtHard  = 25; 
//const float ZBosEvent::sMinZEleCandPtHard  = 24;// systematics lower pT cut 
//const float ZBosEvent::sMinZEleCandPtHard  = 26;// systematics higher pT cut 
const float ZBosEvent::sMinRecoilPt        = 0.5; // Minimum P_T of the recoil before correction
const float ZBosEvent::sMaxRecoilPt        = 40;  // Maximum P_T of the recoil before correction
//Xiaoxuan, set 50 to 73 later
const float ZBosEvent::sMinZMass           = 73;  // Zmass -20%
//const float ZBosEvent::sMinZMass           = 50;//for bkg check,Xiaoxuan

//const float ZBosEvent::sMaxZMass           = 109.4; // Zmass +20%
const float ZBosEvent::sMaxZMass           = 114;



ZBosEvent::ZBosEvent(float minTrackPt, int RhicRunId) : VecBosEvent()
   , mZBosMass(91.1876)
   , mCand1P3()
   , mCand2P3()
{
   VecBosEvent::sMinRecoilTrackPt = minTrackPt;
   VecBosEvent::sRhicRunId   = RhicRunId;
   //cout << "sMinRecoilTrackPt: " <<  sMinRecoilTrackPt << endl;
}


ZBosEvent::~ZBosEvent()
{
}


TVector3       ZBosEvent::GetCandidate1_P3() const { return mCand1P3; }
TVector3       ZBosEvent::GetCandidate2_P3() const { return mCand2P3; }
TVector3       ZBosEvent::GetCandidate2_sameCharge_neg_P3() const { return mCand2P3_sameCharge_negative; }
TVector3       ZBosEvent::GetCandidate2_sameCharge_pos_P3() const { return mCand2P3_sameCharge_positive; }
TLorentzVector ZBosEvent::GetCandidate1_P4() const { return mP4Cand1; }
TLorentzVector ZBosEvent::GetCandidate2_P4() const { return mP4Cand2; }
TLorentzVector ZBosEvent::GetElectronCand_P4() const { return mP4ElectronCand; }
TLorentzVector ZBosEvent::GetPositronCand_P4() const { return mP4PositronCand; }
TVector3       ZBosEvent::GetVecBosonP3() const { return GetCandidate1_P3() + GetCandidate2_P3(); }
TLorentzVector ZBosEvent::GetVecBosonP4() const { return mP4ZBoson; }
TLorentzVector ZBosEvent::GetVecBosonP4_sameCharge() const { return mP4ZBosonSameCharge; }

VecBosTrack&   ZBosEvent::GetElectronTrack() const { return *(*mTracksCandidate.begin()); }


/**
 * The primary method to identify and reconstruct the event with a Z boson.
 */
void ZBosEvent::Process(int McType)
{
   cout << "Monte Carlo Type (0-> data, 1-> W+, 2-> W-, 3-> Z0/other) = "  << McType  << endl;
   UShort_t vertexId = 0;

   VecBosVertexPtrSetIter iVertex = mVertices.begin();
   for ( ; iVertex != mVertices.end(); ++iVertex, vertexId++)
   {
      VecBosVertex &vertex = **iVertex;
      vertex.Process();
      vertex.mId = vertexId;

      if ( !vertex.IsGood()) continue;

      mNumGoodVertices++;

      VecBosVertexPtrSetIter iVertex2 = iVertex; // initialize with the current one
      ++iVertex2; // advance to the next one
      for ( ; iVertex2 != mVertices.end(); ++iVertex2) {
         VecBosVertex &vertex2 = **iVertex2;
         if ( !vertex2.IsGood()) continue;

         Double_t deltaZ = fabs(vertex.mPosition.Z() - vertex2.mPosition.Z());

         if (deltaZ < mMinVertexDeltaZ || mMinVertexDeltaZ < 0)
            mMinVertexDeltaZ = deltaZ;
      }
   }

   // Process tracks
   VecBosTrackPtrSetIter iTrack = mTracks.begin();
   for ( ; iTrack != mTracks.end(); ++iTrack)
   {
      VecBosTrack &track = **iTrack;
      track.Process();

      if (track.IsGood())     mNumGoodTracks++;
      if (track.IsBTrack())   mNumBTracks++;
      if (track.IsETrack())   mNumETracks++;
      if (track.HasCluster()) mNumWithClusterTracks++;
      if (track.IsIsolated()) {
         mNumIsolatedTracks++;

         // if ( track.IsUnBalanced() ) track.FindClosestJet();
      }

      if ( track.IsZelectronCandidate() ) {
         mTracksCandidate.insert(*iTrack);
      }
   }

   // Make sure two track candidate exists
   if (mTracksCandidate.size() <= 1) return;

   VecBosTrack &trackCand1  = **mTracksCandidate.begin();

   mCand1P3 = trackCand1.GetP3EScaled();
   mP4Cand1.SetPxPyPzE(mCand1P3.Px(), mCand1P3.Py(), mCand1P3.Pz(), trackCand1.mCluster2x2.mEnergy);

   EOverP_Cand1 = trackCand1.mCluster2x2.mEnergy/trackCand1.mP3AtDca.Mag();

   mP4Cand2.SetPxPyPzE(0, 0, 0, 0);
   mP4ElectronCand.SetPxPyPzE(0, 0, 0, 0);
   mP4PositronCand.SetPxPyPzE(0, 0, 0, 0);
   EOverP_ElectronCand = 0;
   EOverP_PositronCand = 0;

   if (trackCand1.mStMuTrack->charge() == -1) {
      mP4ElectronCand.SetPxPyPzE(mCand1P3.Px(), mCand1P3.Py(), mCand1P3.Pz(), trackCand1.mCluster2x2.mEnergy);
      EOverP_ElectronCand = trackCand1.mCluster2x2.mEnergy/trackCand1.mP3AtDca.Mag();
   }
   else if (trackCand1.mStMuTrack->charge() == 1) {
      mP4PositronCand.SetPxPyPzE(mCand1P3.Px(), mCand1P3.Py(), mCand1P3.Pz(), trackCand1.mCluster2x2.mEnergy);
      EOverP_PositronCand = trackCand1.mCluster2x2.mEnergy/trackCand1.mP3AtDca.Mag();
   }

   // Loop over candidates.
   VecBosTrackPtrSetIter iTrackCand = mTracksCandidate.begin();
   for (; iTrackCand != mTracksCandidate.end(); ++iTrackCand) {
      VecBosTrack &cand = **iTrackCand;

      if (cand == trackCand1) continue;
      
      if (cand.mStMuTrack->charge() != trackCand1.mStMuTrack->charge()) {  // OPPOSITE CHARGE
         // If the sign of the second track is different from the sign
         // of the first track pick it as Z second electron candidate.


	 //TEST ----------------------------------------------------------------------------------------------
         //TEST! To validate the W recostruction algorithm...
	 // Calculates the vector sum of all tracks in the event. The track is included if it comes from the
	 // same vertex as the lepton candidate and is not the lepton candidate.
		
	 //CalcRecoilFromTracksTest(trackCand1, cand);

         // Calculate the vector sum of all tracks in the event
	 VecBosTrackPtrSetIter iTrack = mTracks.begin();
	 for (; iTrack != mTracks.end(); ++iTrack) {
	    VecBosTrack &track = **iTrack;

	    if ( track.mVertex != trackCand1.mVertex ) continue;
	    if ( track == trackCand1 || track == cand ) continue;

	    if ( track.mP3AtDca.Pt() < sMinRecoilTrackPt ) continue;

	    mP3TrackRecoilTpc_Test   += track.mP3AtDca;
	    mNumRecoilTracksTpc += 1;

	    if ( track.HasCluster() ) {
	      //mP3TrackRecoilTow_Test += track.GetP3EScaled();
	    }
	    else {
	      //mP3TrackRecoilTow_Test += track.mP3AtDca;
	    }
	 }

	 // Process un-tracked BTOW hits
	 for (int iBTow = 0; iBTow < mxBtow; iBTow++)
	   {
	     double towerEnergy = bemc.eneTile[kBTow][iBTow];

	     if (towerEnergy <= 0.200) continue; // skip towers with energy below noise

	     // Correct BCal tower position to the vertex position
	     TVector3 towerP3 = gBCalTowerCoords[iBTow] -  trackCand1.mVertex->mPosition;
	     towerP3.SetMag(towerEnergy); // it is a 3-momentum in the event ref frame
	     TVector3 towCoord = gBCalTowerCoords[iBTow];

	     bool hasMatch            = false;
	     bool partOfElecCandidate = false;

	     // Check if the tower belongs to the electron 2x2 candidate
	     TVector3 distToCluster1 = trackCand1.mCluster2x2.position - towCoord;
	     TVector3 distToCluster2 = cand.mCluster2x2.position - towCoord;

	     if (distToCluster1.Mag() <= 2 * VecBosTrack::sMaxTrackClusterDist)
	       partOfElecCandidate = true;

	     if (distToCluster2.Mag() <= 2 * VecBosTrack::sMaxTrackClusterDist)
	       partOfElecCandidate = true;

	     // Loop over tracks too and exclude towers with a matching track
	     VecBosTrackPtrSetIter iTr = mTracks.begin();
	     for ( ; iTr != mTracks.end(); ++iTr)
	       {
		 VecBosTrack &tr           = **iTr;
		 TVector3     trP3         = tr.mP3AtDca;
		 TVector3     trCoorAtBTow = tr.GetCoordAtBTow();
		 //printf("Track coordinate at  BTower: %f\n", trCoorAtBTow.Mag() );

		 if (trCoorAtBTow.Mag() == 0.0) continue; // track does not extend to barrel

		 // Spacial separation (track - cluster)
		 TVector3 distToTower = trCoorAtBTow - towCoord;

		 if (distToTower.Mag() <= VecBosTrack::sMaxTrackClusterDist) {
		   hasMatch = true; // the TPC track maches to the tower
		   break;
		 }
	       }

	     if (!hasMatch && !partOfElecCandidate) {
	       mP3TrackRecoilNeutrals_Test += towerP3;
	     }
	   }

	 mP3TrackRecoilTpcNeutrals_Test = mP3TrackRecoilTpc_Test + mP3TrackRecoilNeutrals_Test;
	 VecBosEvent::mP3TrackRecoilNeutrals =  mP3TrackRecoilNeutrals_Test;
	 VecBosEvent::mP3TrackRecoilTpcNeutrals =  mP3TrackRecoilTpcNeutrals_Test;

	 VecBosEvent::CalcRecoilCorrected(1);

 	 // END TEST ---------------------------------------------------------------------------------


         mCand2P3 = cand.GetP3EScaled();
         mP4Cand2.SetPxPyPzE(mCand2P3.Px(), mCand2P3.Py(), mCand2P3.Pz(), cand.mCluster2x2.mEnergy);

         if (cand.mStMuTrack->charge() == -1) {
            mP4ElectronCand.SetPxPyPzE(mCand2P3.Px(), mCand2P3.Py(), mCand2P3.Pz(), cand.mCluster2x2.mEnergy);
            EOverP_ElectronCand = trackCand1.mCluster2x2.mEnergy/trackCand1.mP3AtDca.Mag();
         }
         else if (cand.mStMuTrack->charge() == 1) {
            mP4PositronCand.SetPxPyPzE(mCand2P3.Px(), mCand2P3.Py(), mCand2P3.Pz(), cand.mCluster2x2.mEnergy);
            EOverP_PositronCand = trackCand1.mCluster2x2.mEnergy/trackCand1.mP3AtDca.Mag();
         }
           break;
         }
      else {  //SAME CHARGE (for test) 

         if (cand.mStMuTrack->charge() == -1) {
	   mCand2P3_sameCharge_negative = cand.GetP3EScaled();
	   mP4Cand2_sameCharge_negative.SetPxPyPzE(mCand2P3_sameCharge_negative.Px(), mCand2P3_sameCharge_negative.Py(), mCand2P3_sameCharge_negative.Pz(), cand.mCluster2x2.mEnergy);
	   mP4ZBosonSameCharge = mP4Cand1 + mP4Cand2_sameCharge_negative;
         }
         else if (cand.mStMuTrack->charge() == 1) {
	   mCand2P3_sameCharge_positive = cand.GetP3EScaled();
	   mP4Cand2_sameCharge_positive.SetPxPyPzE(mCand2P3_sameCharge_positive.Px(), mCand2P3_sameCharge_positive.Py(), mCand2P3_sameCharge_positive.Pz(), cand.mCluster2x2.mEnergy);
	   mP4ZBosonSameCharge = mP4Cand1 + mP4Cand2_sameCharge_positive;
         }    

         Info("Print", "this is not a Z0 event (decay electrons have same charge!) ");
         break;
      } // END TEST for same charge

   }  // end loop over candidates

   if (McType == 0) { // only for data (reduces file-size output) 
     if (mP4ElectronCand.Pt()  > 10  &&  mP4PositronCand.Pt() > 10 ) {  // S.F. June 11, 2018 - only candidates with a minimum Pt of 10 GeV  
         mHasTwoHighPtLeptons == true;
     } else  mHasTwoHighPtLeptons == false;
   }
   
   CalcZBosP4();

   //ProcessPersistent();
}


void ZBosEvent::ProcessPersistent()
{
   //VecBosEvent::ProcessPersistent();

   // Proceed only if this is a Z event, i.e. it conforms to Z event signature
   //if ( !PassedCutZBos(sMinZEleCandPtLight) ) return;
   //if ( !HasCandidateEle() ) return;

   ////CalcZBosP4();

}


void ZBosEvent::ProcessMC(int McType)
{
   cout << "Monte Carlo Type (1-> W+, 2-> W-, 3-> Z0/other) = "  << McType  << endl;

   fIsMc = true;
   StMcEvent *stMcEvent = (StMcEvent *) StMaker::GetChain()->GetDataSet("StMcEvent");
   assert(stMcEvent);

   mMcEvent = new ZBosMcEvent();
   ((ZBosMcEvent*) mMcEvent)->GetPythiaEvent(*stMcEvent);
}

/**TEST! To validate the W recostruction algorithm...
 * Calculates the vector sum of all tracks in the event. The track is included if it comes from the
 * same vertex as the lepton candidate and is not the lepton candidate.
 */
/*
void ZBosEvent::CalcRecoilFromTracksTest(VecBosTrack cand1, VecBosTrack cand2)
{

   cout << "sMinRecoilTrackPt = " << sMinRecoilTrackPt << endl;

   // Calculate the vector sum of all tracks in the event
   VecBosTrackPtrSetIter iTrack = mTracks.begin();
   for (; iTrack != mTracks.end(); ++iTrack)
   {
      VecBosTrack &track = **iTrack;

      if ( track.mVertex != cand1.mVertex ) continue;
      if ( track.mVertex != cand2.mVertex ) continue;
      if ( track == cand1 || track == cand2 ) continue;

      if ( track.mP3AtDca.Pt() < sMinRecoilTrackPt ) continue;

      mP3TrackRecoilTpc_Test   += track.mP3AtDca;
      mNumRecoilTracksTpc += 1;

      if ( track.HasCluster() ) {
         mP3TrackRecoilTow_Test += track.GetP3EScaled();
      }
      else {
         mP3TrackRecoilTow_Test += track.mP3AtDca;
      }
   }

   // Process un-tracked BTOW hits
   for (int iBTow = 0; iBTow < mxBtow; iBTow++)
   {
      double towerEnergy = bemc.eneTile[kBTow][iBTow];

      if (towerEnergy <= 0.200) continue; // skip towers with energy below noise

      // Correct BCal tower position to the vertex position
      TVector3 towerP3 = gBCalTowerCoords[iBTow] -  cand1.mVertex->mPosition;
      towerP3.SetMag(towerEnergy); // it is a 3-momentum in the event ref frame
      TVector3 towCoord = gBCalTowerCoords[iBTow];

      bool hasMatch            = false;
      bool partOfElecCandidate = false;

      // Check if the tower belongs to the electron 2x2 candidate
      TVector3 distToCluster = cand1.mCluster2x2.position - towCoord;

      if (distToCluster.Mag() <= 2 * VecBosTrack::sMaxTrackClusterDist)
         partOfElecCandidate = true;

      // Loop over tracks too and exclude towers with a matching track
      VecBosTrackPtrSetIter iTr = mTracks.begin();
      for ( ; iTr != mTracks.end(); ++iTr)
      {
         VecBosTrack &tr           = **iTr;
         TVector3     trP3         = tr.mP3AtDca;
         TVector3     trCoorAtBTow = tr.GetCoordAtBTow();
         //printf("Track coordinate at  BTower: %f\n", trCoorAtBTow.Mag() );

         if (trCoorAtBTow.Mag() == 0.0) continue; // track does not extend to barrel

         //if (trP3.DeltaR(towerP3) < 0.1) hasMatch = true; // Checks for a track matching the tower
         //if (hasMatch == true) continue;
         //if ( mTracks->ExtendTrack2Barrel() == false) continue;

         // Spacial separation (track - cluster)
         TVector3 distToTower = trCoorAtBTow - towCoord;

         if (distToTower.Mag() <= VecBosTrack::sMaxTrackClusterDist) {
            hasMatch = true; // the TPC track maches to the tower
            break;
         }
      }

      if (!hasMatch && !partOfElecCandidate) {
         mP3TrackRecoilNeutrals_Test += towerP3;
      }
   }

   mP3TrackRecoilTpcNeutrals_Test = mP3TrackRecoilTpc_Test + mP3TrackRecoilNeutrals_Test;
}
*/

void ZBosEvent::Clear(const Option_t* opt)
{
   VecBosEvent::Clear();
   mCand1P3.SetXYZ(0, 0, 0);
   mCand2P3.SetXYZ(0, 0, 0);
   mCand2P3_sameCharge_positive.SetXYZ(0, 0, 0);
   mCand2P3_sameCharge_negative.SetXYZ(0, 0, 0);
   mP4Cand1.SetPxPyPzE(0, 0, 0, 0);
   mP4Cand2.SetPxPyPzE(0, 0, 0, 0);
   mP4Cand2_sameCharge_positive.SetPxPyPzE(0, 0, 0, 0);
   mP4Cand2_sameCharge_negative.SetPxPyPzE(0, 0, 0, 0);
   mP4ElectronCand.SetPxPyPzE(0, 0, 0, 0);
   mP4PositronCand.SetPxPyPzE(0, 0, 0, 0);
   mP3TrackRecoilTpc_Test.SetXYZ(0, 0, 0);
   //mP3TrackRecoilTow_Test.SetXYZ(0, 0, 0);
   mP3TrackRecoilNeutrals_Test.SetXYZ(0, 0, 0);
   mP3TrackRecoilTpcNeutrals_Test.SetXYZ(0, 0, 0);

}


void ZBosEvent::Print(const Option_t* opt) const
{
   Info("Print", ":");
   //VecBosEvent::Print(opt);
   mCand1P3.Print();
   mCand2P3.Print();
}


bool ZBosEvent::PassedCutZBos(float minElePt) const
{
   if ( HasCandidateEle() &&
        mCand1P3.Pt() >= minElePt &&
        mCand2P3.Pt() >= minElePt )
   {
      return true;
   }

   return false;
}


bool ZBosEvent::PassedCutZMass(float minElePt) const
{
   if ( PassedCutZBos(minElePt)      &&
        mP4ZBoson.M() > sMinZMass    &&
        mP4ZBoson.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}


bool ZBosEvent::PassedCutZSameChargePos(float minElePt) const  //for test
{
   if ( HasCandidateEle() &&
        mCand1P3.Pt() >= minElePt &&
        mCand2P3_sameCharge_positive.Pt() >= minElePt &&
        mP4ZBosonSameCharge.M() > sMinZMass    &&
        mP4ZBosonSameCharge.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}
//Xiaoxuan, begins
bool ZBosEvent::PassedCutZSameChargePosZMassExt(float minElePt) const
{
   if ( PassedCutZSameChargePos(minElePt)      &&
        mP4ZBoson.M() > 50           &&
        mP4ZBoson.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}
//Xiaoxuan,ends

bool ZBosEvent::PassedCutZSameChargeNeg(float minElePt) const  //for test
{
   if ( HasCandidateEle() &&
        mCand1P3.Pt() >= minElePt &&
        mCand2P3_sameCharge_negative.Pt() >= minElePt &&
        mP4ZBosonSameCharge.M() > sMinZMass    &&
        mP4ZBosonSameCharge.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}
//Xiaoxuan,begins
bool ZBosEvent::PassedCutZSameChargeNegZMassExt(float minElePt) const
{
   if ( PassedCutZSameChargeNeg(minElePt)      &&
        mP4ZBoson.M() > 50           &&
        mP4ZBoson.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}
//Xiaoxuan,ends



bool ZBosEvent::PassedCutZRecoilTest(float minElePt) const
{
   if ( PassedCutZBos(minElePt)      &&
        mP3TrackRecoilTpcNeutrals.Pt() > sMinRecoilPt &&
        mP3TrackRecoilTpcNeutralsCorrected.Pt() > sMinRecoilPt &&
        mP3TrackRecoilTpcNeutrals.Pt() < sMaxRecoilPt &&
        mP3TrackRecoilTpcNeutralsCorrected.Pt() < sMaxRecoilPt &&
        mP4ZBoson.M() > sMinZMass    &&
        mP4ZBoson.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}


bool ZBosEvent::PassedCutZMassExt(float minElePt) const
{
   if ( PassedCutZBos(minElePt)      &&
        mP4ZBoson.M() > 50           &&
        mP4ZBoson.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}

bool ZBosEvent::PassedCutZMass80(float minElePt) const
{
   if ( PassedCutZBos(minElePt)      &&
        mP4ZBoson.M() > 80           &&
        mP4ZBoson.M() < sMaxZMass )
   {
      return true;
   }

   return false;
}


void ZBosEvent::CalcZBosP4()
{
   mP4ZBoson = mP4Cand1 + mP4Cand2;
}


void ZBosEvent::Streamer(TBuffer &R__b)
{

   if (R__b.IsReading()) {
      //Info("Streamer", "Reading...");
      R__b.ReadClassBuffer(ZBosEvent::Class(), this);
   }
   else {
      //Info("Streamer", "Writing... ");
      R__b.WriteClassBuffer(ZBosEvent::Class(), this);
   }

}
