#ifndef WBosEventHContainerNoEndcap_h
#define WBosEventHContainerNoEndcap_h

#include "TDirectoryFile.h"

#include "utils/PlotHelper.h"
#include "utils/ProtoEvent.h"


/**
 *
 */
class WBosEventHContainerNoEndcap : public PlotHelper
{
public:

   WBosEventHContainerNoEndcap();
   WBosEventHContainerNoEndcap(TDirectory *dir);

   using PlotHelper::FillDerived;
   using PlotHelper::PostFill;

   void Fill(ProtoEvent &ev);
   void FillDerived();
   void PostFill();

private:

   void BookHists();


   ClassDef(WBosEventHContainerNoEndcap, 1)
};

#endif
