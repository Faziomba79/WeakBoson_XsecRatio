#include "Z0EventHContainer.h"

#include "TF1.h"
#include "TF2.h"

#include "ZBosEvent.h"
#include "TrackHContainer.h"


ClassImp(Z0EventHContainer)

using namespace std;


/** Default constructor. */
Z0EventHContainer::Z0EventHContainer() : PlotHelper()
{
   BookHists();
}


Z0EventHContainer::Z0EventHContainer(TDirectory *dir) : PlotHelper(dir)
{
   BookHists();
}


/** */
void Z0EventHContainer::BookHists()
{
   string shName;
   TH1*   h;

   Double_t xBinsPt2016Pr[4]  = {0, 5, 10, 25};
   Double_t xBinsRap2016Pr[4] = {-0.6, -0.2, 0.2, 0.6};
   Double_t xBinsPt_XSecBins[12]  = {0, 1.25, 2.5, 3.75, 5, 7.5, 10, 12.5, 15, 17.5, 20, 25};

   fDir->cd();

   o["hRunId"]                 = h = new TH1I("hRunId", "; Run Id; Events", 20, 0, 20);
   h->SetOption("hist GRIDX");
   o["hZdcRate"]               = h = new TH1I("hZdcRate", "; ZDC Rate; Events", 50, 100e3, 200e3);
   h->SetOption("hist GRIDX");
   o["hNumJets"]               = h = new TH1I("hNumJets", "; Num. of Jets; Events", 15, 0, 15);
   h->SetOption("hist GRIDX");
   o["hNumJetsRecoil"]         = h = new TH1I("hNumJetsRecoil", "; Num. of Jets in Recoil; Events", 15, 0, 15);
   h->SetOption("hist GRIDX");
   o["hNumJetsWithIsoTrack"]   = h = new TH1I("hNumJetsWithIsoTrack", "; Num. of Jets w/ Iso Track; Events", 15, 0, 15);
   h->SetOption("hist GRIDX");
   o["hNumVertices"]           = h = new TH1I("hNumVertices", "; Num. of Vertices; Events", 10, 0, 10);
   h->SetOption("hist GRIDX");
   o["hNumGoodVertices"]       = h = new TH1I("hNumGoodVertices", "; Num. of Good Vertices; Events", 10, 0, 10);
   h->SetOption("hist GRIDX");
   o["hNumTracks"]             = h = new TH1I("hNumTracks", "; Num. of Tracks; Events", 50, 0, 250);
   h->SetOption("hist GRIDX");
   o["hNumGoodTracks"]         = h = new TH1I("hNumGoodTracks", "; Num. of Good Tracks; Events", 40, 0, 40);
   h->SetOption("hist GRIDX");
   o["hNumBTracks"]            = h = new TH1I("hNumBTracks", "; Num. of Barrel Tracks; Events", 40, 0, 40);
   h->SetOption("hist GRIDX");
   o["hNumETracks"]            = h = new TH1I("hNumETracks", "; Num. of Endcap Tracks; Events", 10, 0, 10);
   h->SetOption("hist GRIDX");
   o["hNumWithClusterTracks"]  = h = new TH1I("hNumWithClusterTracks", "; Num. of Tracks w/ Cluster; Events", 10, 0, 10);
   h->SetOption("hist GRIDX");
   o["hNumIsolatedTracks"]     = h = new TH1I("hNumIsolatedTracks", "; Num. of Isolated Tracks; Events", 10, 0, 10);
   h->SetOption("hist GRIDX");
   o["hNumCandidateTracks"]    = h = new TH1I("hNumCandidateTracks", "; Num. of Candidate Tracks; Events", 10, 0, 10);
   h->SetOption("hist GRIDX");
   o["hNumTracksWithBCluster"] = h = new TH1I("hNumTracksWithBCluster", "; Num. of Tracks with Barrel Cluster; Events", 5, 0, 5);
   h->SetOption("hist GRIDX");

   o["hCandidateTrackPt"]          = h = new TH1F("hCandidateTrackPt", "; Track P_T; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hCandidate1E"]   = h = new TH1F("hCandidate1E", "; E; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2E"]   = h = new TH1F("hCandidate2E", "; E; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate1Pt"]   = h = new TH1F("hCandidate1Pt", "; P_T; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2Pt"]   = h = new TH1F("hCandidate2Pt", "; P_T; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2_sameCharge_pos_Pt"]   = h = new TH1F("hCandidate2_sameCharge_pos_Pt", "; P_T; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2_sameCharge_neg_Pt"]   = h = new TH1F("hCandidate2_sameCharge_neg_Pt", "; P_T; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate1Pz"]   = h = new TH1F("hCandidate1Pz", "; P_T; Events", 40, -60, 60);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2Pz"]   = h = new TH1F("hCandidate2Pz", "; P_T; Events", 40, -60, 60);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate1Eta"]   = h = new TH1F("hCandidate1Eta", ";Candidate 1 - #eta; Events", 20, -2, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2Eta"]   = h = new TH1F("hCandidate2Eta", ";Candidate 2 - #eta; Events", 20, -2, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2_sameCharge_pos_Eta"]   = h = new TH1F("hCandidate2_sameCharge_pos_Eta", ";Candidate 2 - #eta; Events", 20, -2, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hCandidate2_sameCharge_neg_Eta"]   = h = new TH1F("hCandidate2_sameCharge_neg_Eta", ";Candidate 2 - #eta; Events", 20, -2, 2);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hElectronCandE"]   = h = new TH1F("hElectronCandE", "; E^{e-}; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hPositronCandE"]   = h = new TH1F("hPositronCandE", "; E^{e+}; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hElectronCandPt"]   = h = new TH1F("hElectronCandPt", "; P_T^{e-}; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hPositronCandPt"]   = h = new TH1F("hPositronCandPt", "; P_T^{e+}; Events", 80, 0, 80);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hElectronCandPz"]   = h = new TH1F("hElectronCandPz", "; P_T^{e-}; Events", 40, -60, 60);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hPositronCandPz"]   = h = new TH1F("hPositronCandPz", "; P_T^{e+}; Events", 40, -60, 60);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hElectronCandEta"]   = h = new TH1F("hElectronCandEta", ";#eta^{e-}; Events", 20, -2, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hPositronCandEta"]   = h = new TH1F("hPositronCandEta", ";#eta^{e+}; Events", 20, -2, 2);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hZ0_MassInv"]   = h = new TH1F("hZ0_MassInv", "; M_{Z^{0}} (GeV/c^{2}); Events", 20, 73, 114);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_MassInv2"]   = h = new TH1F("hZ0_MassInv2", "; M_{Z^{0}} (GeV/c^{2}); Events", 50, 10, 120);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_MassInv2_sameCharge"]   = h = new TH1F("hZ0_MassInv2_sameCharge", "; M_{Z^{0}} (GeV/c^{2}); Events", 50, 10, 120);
   h->SetOption("hist GRIDX GRIDY XY");
   //Xiaoxuan for samecahrge Z0 pT
   o["hZ0_Pt_sameCharge"]   = h = new TH1F("hZ0_Pt_sameCharge", "; Z^{0}-P_{T} (GeV/c); Events", 20, 0, 25);
   h->SetOption("hist GRIDX GRIDY XY");
   //Xiaoxuan
   o["hZ0_Pt"]   = h = new TH1F("hZ0_Pt", "; Z^{0}-P_{T} (GeV/c); Events", 20, 0, 25);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_zoomin"]   = h = new TH1F("hZ0_Pt_zoomin", "; Z^{0}-P_{T} (GeV/c); Events", 10, 0, 10);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_zoomin20b"]   = h = new TH1F("hZ0_Pt_zoomin20b", "; Z^{0}-P_{T} (GeV/c); Events", 20, 0, 10);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_2016Pr"]   = h = new TH1D("hZ0_Pt_2016Pr", "; Z^{0}-P_{T} (GeV/c); Events", 3, xBinsPt2016Pr);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hZ0_Pt_XSecBins"]  = h = new TH1D("hZ0_Pt_XSecBins", "; Z^{0}-P_{T} (GeV/c); Events", 11, xBinsPt_XSecBins);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin1"]  = h = new TH1D("hZ0_Pt_XSecBin1", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[0], xBinsPt_XSecBins[1]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin2"]  = h = new TH1D("hZ0_Pt_XSecBin2", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[1], xBinsPt_XSecBins[2]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin3"]  = h = new TH1D("hZ0_Pt_XSecBin3", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[2], xBinsPt_XSecBins[3]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin4"]  = h = new TH1D("hZ0_Pt_XSecBin4", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[3], xBinsPt_XSecBins[4]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin5"]  = h = new TH1D("hZ0_Pt_XSecBin5", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[4], xBinsPt_XSecBins[5]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin6"]  = h = new TH1D("hZ0_Pt_XSecBin6", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[5], xBinsPt_XSecBins[6]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin7"]  = h = new TH1D("hZ0_Pt_XSecBin7", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[6], xBinsPt_XSecBins[7]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin8"]  = h = new TH1D("hZ0_Pt_XSecBin8", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[7], xBinsPt_XSecBins[8]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin9"]  = h = new TH1D("hZ0_Pt_XSecBin9", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[8], xBinsPt_XSecBins[9]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin10"]  = h = new TH1D("hZ0_Pt_XSecBin10", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[9], xBinsPt_XSecBins[10]);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pt_XSecBin11"]  = h = new TH1D("hZ0_Pt_XSecBin11", "; Z^{0}-P_{T} (GeV/c); Events", 20, xBinsPt_XSecBins[10], xBinsPt_XSecBins[11]);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hZ0_Pz"]          = h = new TH1D("hZ0_Pz", "; Z^{0}-P_{z}; Events", 30, -60, 60);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Pz_zoomin"]   = h = new TH1D("hZ0_Pz_zoomin", "; Z^{0}-P_{z}; Events", 10, -20, 20);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Eta"]   = h = new TH1F("hZ0_Eta", "; Z^{0}- #eta; Events", 20, -4, 4);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Rapidity"]   = h = new TH1F("hZ0_Rapidity", "; Z^{0}- y; Events", 20, -2, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Rapidity_2016Pr"]   = h = new TH1D("hZ0_Rapidity_2016Pr", "; Z^{0}- y; Events", 3, xBinsRap2016Pr);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hZ0_Phi"]        = h = new TH1F("hZ0_Phi", "  ; Z^{0} #phi; Events", 16, -M_PI, M_PI);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hTrackRecoilPt"]             = h = new TH1F("hTrackRecoilPt", "Recoil from Tracks: TPC+TOW; Track-based Recoil P_{T}; Events;", 40, 0, 40);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hTrackRecoilTpcPt"]          = h = new TH1F("hTrackRecoilTpcPt", "Recoil from Tracks: TPC only; Track-based Recoil P_{T}; Events", 40, 0, 40);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hTrackRecoilTpcPt_zoomin"] = h = new TH1F("hTrackRecoilTpcPt_zoomin", "Recoil from Tracks: TPC only; Track-based Recoil P_{T}; Events", 20, 0, 10);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hTrackRecoilWithNeutralsPt"] = h = new TH1F("hTrackRecoilWithNeutralsPt", "Recoil from Tracks: TPC+emCal (also trackless clusters) ; Track-based Recoil P_{T}; Events", 40, 0, 40);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hTrackRecoilWithNeutralsPtCorrected"] = h = new TH1F("hTrackRecoilWithNeutralsPtCorrected", "Recoil from Tracks: TPC+emCal (CORRECTED) ; Track-based Recoil P_{T}; Events", 40, 0, 40);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hTrackRecoilWithNeutralsPt_zoomin"] = h = new TH1F("hTrackRecoilWithNeutralsPt_zoomin", "Recoil from Tracks: TPC+emCal (also trackless clusters) ; Track-based Recoil P_{T}; Events", 20, 0, 10);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hTrackRecoilWithNeutralsPtCorrected_zoomin"] = h = new TH1F("hTrackRecoilWithNeutralsPtCorrected_zoomin", "Recoil from Tracks: TPC+emCal (CORRECTED) ; Track-based Recoil P_{T}; Events", 20, 0, 10);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hRecoTestAbsoluteDiff_Pt"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt", "Test of Boson Reconstruction ; Z^{0} P_{T} (recoil-leptons) [GeV]; Events", 20, -10, 10);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hRecoTestRelativeDiff_Pt"] = h = new TH1F("hRecoTestRelativeDiff_Pt", "Test of Boson Reconstruction ; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -0.5, 0.5);
   h->SetOption("hist GRIDX GRIDY XY");

   o["hRecoTestAbsoluteDiff_Pt_bin1"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin1", "Test of Boson Reconstruction bin1; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin2"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin2", "Test of Boson Reconstruction bin2; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin3"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin3", "Test of Boson Reconstruction bin3; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin4"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin4", "Test of Boson Reconstruction bin4; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin5"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin5", "Test of Boson Reconstruction bin5; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin6"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin6", "Test of Boson Reconstruction bin6; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin7"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin7", "Test of Boson Reconstruction bin7; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin8"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin8", "Test of Boson Reconstruction bin8; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin9"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin9", "Test of Boson Reconstruction bin9; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);
   o["hRecoTestAbsoluteDiff_Pt_bin10"] = h = new TH1F("hRecoTestAbsoluteDiff_Pt_bin10", "Test of Boson Reconstruction bin10; Z^{0} P_{T} (recoil-leptons/leptons) [GeV]; Events", 20, -10, 10);

   o["hRecoTestRelativeDiff_Pt_bin1"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin1", "Test of Boson Reconstruction bin1; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin2"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin2", "Test of Boson Reconstruction bin2; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin3"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin3", "Test of Boson Reconstruction bin3; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin4"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin4", "Test of Boson Reconstruction bin4; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin5"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin5", "Test of Boson Reconstruction bin5; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin6"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin6", "Test of Boson Reconstruction bin6; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin7"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin7", "Test of Boson Reconstruction bin7; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin8"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin8", "Test of Boson Reconstruction bin8; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin9"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin9", "Test of Boson Reconstruction bin9; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);
   o["hRecoTestRelativeDiff_Pt_bin10"] = h = new TH1F("hRecoTestRelativeDiff_Pt_bin10", "Test of Boson Reconstruction bin10; Z^{0} P_{T} (recoil-leptons/leptons); Events", 20, -5, 10);

   o["hEOverP_ElectronCand"] = h = new TH1I("hEOverP_ElectronCand", "; E/P; Events", 50, 0, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hEOverP_ElectronCand_25_pt_35"] = h = new TH1I("hEOverP_ElectronCand_25_pt_35", "25 < p^{e}_{T} < 35 GeV; E^{e}/P^{e}; Events", 50, 0, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hEOverP_ElectronCand_35_pt_45"] = h = new TH1I("hEOverP_ElectronCand_35_pt_45", "35 < p^{e}_{T} < 45 GeV; E^{e}/P^{e}; Events", 50, 0, 2);
   h->SetOption("hist GRIDX GRIDY XY");
   o["hEOverP_ElectronCand_45_pt_55"] = h = new TH1I("hEOverP_ElectronCand_45_pt_55", "45 < p^{e}_{T} < 55 GeV; E^{e}/P^{e}; Events", 50, 0, 2);
   h->SetOption("hist GRIDX GRIDY XY");

   d["tracks"] = new TrackHContainer(new TDirectoryFile("tracks", "tracks", "", fDir));
   d["track_ElectronCand"] = new TrackHContainer(new TDirectoryFile("track_ElectronCand", "track_ElectronCand", "", fDir));
}


/** */
void Z0EventHContainer::Fill(ProtoEvent &ev)
{
   ZBosEvent& event = (ZBosEvent&) ev;

   ((TH1*) o["hRunId"])                     ->Fill(event.GetRunId());
   ((TH1*) o["hZdcRate"])                   ->Fill(event.zdcRate);
   ((TH1*) o["hNumJets"])                   ->Fill(event.GetNumJets());
   ((TH1*) o["hNumJetsRecoil"])             ->Fill(event.GetNumJetsRecoil());
   ((TH1*) o["hNumJetsWithIsoTrack"])       ->Fill(event.GetNumJetsWithIsoTrack());
   ((TH1*) o["hNumVertices"])               ->Fill(event.GetNumVertices());
   ((TH1*) o["hNumGoodVertices"])           ->Fill(event.GetNumGoodVertices());
   ((TH1*) o["hNumTracks"])                 ->Fill(event.GetNumTracks());
   ((TH1*) o["hNumGoodTracks"])             ->Fill(event.GetNumGoodTracks());
   ((TH1*) o["hNumBTracks"])                ->Fill(event.GetNumBTracks());
   ((TH1*) o["hNumETracks"])                ->Fill(event.GetNumETracks());
   ((TH1*) o["hNumWithClusterTracks"])      ->Fill(event.GetNumWithClusterTracks());
   ((TH1*) o["hNumIsolatedTracks"])         ->Fill(event.GetNumIsolatedTracks());
   ((TH1*) o["hNumCandidateTracks"])        ->Fill(event.GetNumCandidateTracks());
   ((TH1*) o["hNumTracksWithBCluster"])     ->Fill(event.GetNumTracksWithBCluster());

   ((TH1*) o["hCandidate1E"])    ->Fill(event.GetCandidate1_P4().E());
   ((TH1*) o["hCandidate2E"])    ->Fill(event.GetCandidate2_P4().E());
   ((TH1*) o["hCandidate1Pt"])   ->Fill(event.GetCandidate1_P3().Pt());
   ((TH1*) o["hCandidate2Pt"])   ->Fill(event.GetCandidate2_P3().Pt());
   ((TH1*) o["hCandidate2_sameCharge_pos_Pt"])   ->Fill(event.GetCandidate2_sameCharge_pos_P3().Pt());
   ((TH1*) o["hCandidate2_sameCharge_neg_Pt"])   ->Fill(event.GetCandidate2_sameCharge_neg_P3().Pt());
   ((TH1*) o["hCandidate1Pz"])   ->Fill(event.GetCandidate1_P3().Pz());
   ((TH1*) o["hCandidate2Pz"])   ->Fill(event.GetCandidate2_P3().Pz());
   ((TH1*) o["hElectronCandE"])  ->Fill(event.GetElectronCand_P4().E());
   ((TH1*) o["hPositronCandE"])  ->Fill(event.GetPositronCand_P4().E());
   ((TH1*) o["hElectronCandPt"]) ->Fill(event.GetElectronCand_P4().Pt());
   ((TH1*) o["hPositronCandPt"]) ->Fill(event.GetPositronCand_P4().Pt());
   ((TH1*) o["hElectronCandPz"]) ->Fill(event.GetElectronCand_P4().Pz());
   ((TH1*) o["hPositronCandPz"]) ->Fill(event.GetPositronCand_P4().Pz());
   ((TH1*) o["hZ0_MassInv"])     ->Fill(event.GetVecBosonP4().M());
   ((TH1*) o["hZ0_MassInv2"])    ->Fill(event.GetVecBosonP4().M());
   ((TH1*) o["hZ0_MassInv2_sameCharge"]) ->Fill(event.GetVecBosonP4_sameCharge().M());
   //Xiaoxuan
   ((TH1*) o["hZ0_Pt_sameCharge"]) ->Fill(event.GetVecBosonP4_sameCharge().Pt());
   //Xiaoxxuan
   ((TH1*) o["hZ0_Pt"])          ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_zoomin"])   ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_zoomin20b"])->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_2016Pr"])   ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBins"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin1"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin2"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin3"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin4"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin5"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin6"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin7"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin8"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin9"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin10"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pt_XSecBin11"]) ->Fill(event.GetVecBosonP4().Pt());
   ((TH1*) o["hZ0_Pz"])          ->Fill(event.GetVecBosonP4().Pz());
   ((TH1*) o["hZ0_Pz_zoomin"])   ->Fill(event.GetVecBosonP4().Pz());

   ((TH1*) o["hEOverP_ElectronCand"])    ->Fill(event.EOverP_Cand1);
   
   Double_t Cand1Pt = event.GetElectronCand_P4().Pt();
   if ( Cand1Pt > 25 && Cand1Pt < 35 ) {
      ((TH1*) o["hEOverP_ElectronCand_25_pt_35"])    ->Fill(event.EOverP_Cand1);
   } else if ( Cand1Pt > 35 && Cand1Pt < 45) {
      ((TH1*) o["hEOverP_ElectronCand_35_pt_45"])    ->Fill(event.EOverP_Cand1);
   } else if ( Cand1Pt > 45 && Cand1Pt < 55) {
      ((TH1*) o["hEOverP_ElectronCand_45_pt_55"])    ->Fill(event.EOverP_Cand1);
   }
   
   if ( event.GetCandidate1_P3().Pt() && event.GetCandidate2_P3().Pt() ) {
      ((TH1*) o["hCandidate1Eta"])      ->Fill(event.GetCandidate1_P3().Eta());
      ((TH1*) o["hCandidate2Eta"])      ->Fill(event.GetCandidate2_P3().Eta());
      ((TH1*) o["hElectronCandEta"])    ->Fill(event.GetElectronCand_P4().Eta());
      ((TH1*) o["hPositronCandEta"])    ->Fill(event.GetPositronCand_P4().Eta());
      ((TH1*) o["hZ0_Eta"])             ->Fill(event.GetVecBosonP4().Eta());
      ((TH1*) o["hZ0_Rapidity"])        ->Fill(event.GetVecBosonP4().Rapidity());
      ((TH1*) o["hZ0_Rapidity_2016Pr"]) ->Fill(event.GetVecBosonP4().Rapidity());
      ((TH1*) o["hZ0_Phi"])             ->Fill(event.GetVecBosonP4().Phi());
   }
   
   if ( event.GetCandidate1_P3().Pt() && event.GetCandidate2_sameCharge_neg_P3().Pt() )
      ((TH1*) o["hCandidate2_sameCharge_neg_Eta"])      ->Fill(event.GetCandidate2_sameCharge_neg_P3().Eta());  // for test
   if ( event.GetCandidate1_P3().Pt() && event.GetCandidate2_sameCharge_pos_P3().Pt() )
      ((TH1*) o["hCandidate2_sameCharge_pos_Eta"])      ->Fill(event.GetCandidate2_sameCharge_pos_P3().Eta());  // for test
   
   //((TH1*) o["hTrackRecoilPt"])->Fill(event.GetTrackRecoil().Pt());
   ((TH1*) o["hTrackRecoilTpcPt"])->Fill(event.mP3TrackRecoilTpc_Test.Pt());
   ((TH1*) o["hTrackRecoilTpcPt_zoomin"])->Fill(event.mP3TrackRecoilTpc_Test.Pt());
   ((TH1*) o["hTrackRecoilWithNeutralsPt"])->Fill(event.GetTrackRecoilTpcNeutrals().Pt());
   ((TH1*) o["hTrackRecoilWithNeutralsPtCorrected"])->Fill(event.GetTrackRecoilTpcNeutralsCorrected().Pt());
   ((TH1*) o["hTrackRecoilWithNeutralsPt_zoomin"])->Fill(event.GetTrackRecoilTpcNeutrals().Pt());
   ((TH1*) o["hTrackRecoilWithNeutralsPtCorrected_zoomin"])->Fill(event.GetTrackRecoilTpcNeutralsCorrected().Pt());


// Calculate the relative difference of Z0-pT calculated from the recoil (a la W) and from decay electrons.  
// do it also in each of the pT bins we use for the xsec 
   Double_t ZPt = event.GetVecBosonP4().Pt();

   ((TH1*) o["hRecoTestAbsoluteDiff_Pt"])->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
   ((TH1*) o["hRecoTestRelativeDiff_Pt"])->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);

    if (ZPt >= 0. && ZPt < 2.5) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin1"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin1"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 2.5 && ZPt < 5.0) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin2"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin2"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 5.0 && ZPt < 7.5) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin3"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin3"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 7.5 && ZPt < 10.) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin4"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin4"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 10. && ZPt < 12.5) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin5"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin5"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 12.5 && ZPt < 15.) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin6"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin6"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 15. && ZPt < 17.5) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin7"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin7"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 17.5 && ZPt < 20.) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin8"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin8"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 20. && ZPt < 22.5) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin9"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin9"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    } else if (ZPt >= 22.5 && ZPt < 25.) {
      ((TH1*) o["hRecoTestAbsoluteDiff_Pt_bin10"])     ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt));
      ((TH1*) o["hRecoTestRelativeDiff_Pt_bin10"])    ->Fill((event.GetTrackRecoilTpcNeutralsCorrected().Pt()-ZPt)/ZPt);
    }

   d["tracks"]->Fill(ev);

   //((TrackHContainer*) d["track_candidate"])->Fill(event.GetElectronTrack());
}
