#ifndef Z0AsymHContainer_sameCharge_pos_h
#define Z0AsymHContainer_sameCharge_pos_h

#include "utils/ProtoEvent.h"

#include "AsymHContainer.h"
#include "Globals.h"


/**
 * A container to hold histograms with calculated asymmetries of the Z0 boson.
 */
class Z0AsymHContainer_sameCharge_pos : public AsymHContainer
{
public:

   Z0AsymHContainer_sameCharge_pos();
   Z0AsymHContainer_sameCharge_pos(TDirectory *dir, EAsymType asymType=kAsymPlain);

   void Fill(ProtoEvent &ev);

private:

   void BookHists();


   ClassDef(Z0AsymHContainer_sameCharge_pos, 1)
};

#endif
