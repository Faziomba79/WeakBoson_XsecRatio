#include "ZBosRootFile.h"

#include <climits>
#include <sstream>

#include "St_db_Maker/St_db_Maker.h"
#include "StEmcUtil/database/StBemcTables.h"
#include "StEmcUtil/geometry/StEmcGeom.h"

#include "AllAsymHContainer.h"
#include "AsymHContainer.h"
#include "EventHContainer.h"
#include "EventDisplayHContainer.h"
#include "Z0EventHContainer.h"
#include "Z0_MCHContainer.h"
#include "ZBosEvent.h"
#include "VecBosTrack.h"
#include "VecBosVertex.h"

#include "utils/utils.h"

using namespace std;


ZBosRootFile::ZBosRootFile() : VecBosRootFile()
{
   BookHists();
}


ZBosRootFile::ZBosRootFile(const char *fname, Option_t *option, Int_t isMc, Bool_t isZ, const char *ftitle, Int_t compress) :
  VecBosRootFile(fname, option, isMc, isZ, ftitle, compress)
{
   gBTowGeom = StEmcGeom::instance("bemc");
   gBTowGeom->printGeom();
   BookHists();
}


ZBosRootFile::~ZBosRootFile()
{
}


void ZBosRootFile::BookHists()
{
   // Delete histograms created in parent class
   if (fHists) { delete fHists; fHists = 0; }
   fHistCuts.clear();

   PlotHelper *ph;

   fHists = new PlotHelper(this);

   fHists->d["event_info"] = ph = new EventHContainer(new TDirectoryFile("event_info", "event_info", "", this));
   fHistCuts[kCUT_EVENT_NOCUT].insert(ph);

   fHists->d["event"] = ph = new Z0EventHContainer(new TDirectoryFile("event", "event", "", this));
   fHistCuts[kCUT_EVENT_NOCUT].insert(ph);

   fHists->d["event_z"] = ph = new Z0EventHContainer(new TDirectoryFile("event_z", "event_z", "", this));
   fHistCuts[kCUT_EVENT_Z].insert(ph);

   fHists->d["event_z_samecharge_pos"] = ph = new Z0EventHContainer(new TDirectoryFile("event_z_samecharge_pos", "event_z_samecharge_pos", "", this));
   fHistCuts[kCUT_EVENT_Z_SAME_POS].insert(ph);

   fHists->d["event_z_samecharge_neg"] = ph = new Z0EventHContainer(new TDirectoryFile("event_z_samecharge_neg", "event_z_samecharge_neg", "", this));
   fHistCuts[kCUT_EVENT_Z_SAME_NEG].insert(ph);

//Xiaoxuan
   fHists->d["event_z_samecharge_pos_m50"] = ph = new Z0EventHContainer(new TDirectoryFile("event_z_samecharge_pos_m50", "event_z_samecharge_pos_m50", "", this));
   fHistCuts[kCUT_EVENT_Z_SAME_POS_M50].insert(ph);

   fHists->d["event_z_samecharge_neg_m50"] = ph = new Z0EventHContainer(new TDirectoryFile("event_z_samecharge_neg_m50", "event_z_samecharge_neg_m50", "", this));
   fHistCuts[kCUT_EVENT_Z_SAME_NEG_M50].insert(ph);
//Xiaoxuan

   fHists->d["event_z_m50"] = ph = new Z0EventHContainer(new TDirectoryFile("event_z_m50", "event_z_m50", "", this));
   fHistCuts[kCUT_EVENT_Z_M50].insert(ph);

   fHists->d["event_z_recotest"] = ph = new Z0EventHContainer(new TDirectoryFile("event_z_recotest", "event_z_recotest", "", this));
   fHistCuts[kCUT_EVENT_RECOTEST].insert(ph);

   //if (!fIsMc) return;
   if (fIsMc) { // do it only for Monte Carlo
     if (fIsMc == 3) { // do it only for the Z0 Monte Carlo

         fHists->d["event_mc_nocut"] = ph = new Z0_MCHContainer(new TDirectoryFile("event_mc_nocut", "event_mc_nocut", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_NOCUT].insert(ph); 

         fHists->d["event_mc_z_recotest"] = ph = new Z0_MCHContainer(new TDirectoryFile("event_mc_z_recotest", "event_mc_z_recotest", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_RECOTEST].insert(ph); 

         fHists->d["event_mc_z"] = ph = new Z0_MCHContainer(new TDirectoryFile("event_mc_z", "event_mc_z", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_Z].insert(ph); 
     }
   } else { // do it only for the data

     fHists->d["asym"] = ph = new AllAsymHContainer(new TDirectoryFile("asym", "asym", "", this));
     fHistCuts[kCUT_EVENT_Z].insert(ph);

     //Xiaoxuan
     fHists->d["asym_pos"] = ph = new AllAsymHContainer(new TDirectoryFile("asym_pos", "asym_pos", "", this));
     fHistCuts[kCUT_EVENT_Z_SAME_POS].insert(ph);
     fHists->d["asym_neg"] = ph = new AllAsymHContainer(new TDirectoryFile("asym_neg", "asym_neg", "", this));
     fHistCuts[kCUT_EVENT_Z_SAME_NEG].insert(ph);
     //Xiaoxuan

     fHists->d["asym_m50"] = ph = new AllAsymHContainer(new TDirectoryFile("asym_m50", "asym_m50", "", this));
     fHistCuts[kCUT_EVENT_Z_M50].insert(ph);

     fHists->d["asym_m80"] = ph = new AllAsymHContainer(new TDirectoryFile("asym_m80", "asym_m80", "", this));
     fHistCuts[kCUT_EVENT_Z_M80].insert(ph);

   }

   /*
   if (!fIsMc==3) {
      fHists->d["event_mc"] = ph = new MCHContainer(new TDirectoryFile("event_mc", "event_mc", "", this), fIsMc);
      fHistCuts[kCUT_EVENT_Z].insert(ph);
   }
   */

   this->cd();
}


/** */
void ZBosRootFile::Fill(ProtoEvent &ev)
{
   ZBosEvent& z_event = (ZBosEvent&) ev;

   Fill(ev, kCUT_EVENT_NOCUT);

   if ( z_event.PassedCutZMass(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_Z);
   }

   if ( z_event.PassedCutZMassExt(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_Z_M50);
   }

   if ( z_event.PassedCutZMass80(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_Z_M80);
   }

   if ( z_event.PassedCutZSameChargePos(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_Z_SAME_POS);
      cout<<" This is Z0 with same charge POS"<<endl;
   }

   if ( z_event.PassedCutZSameChargeNeg(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_Z_SAME_NEG);
      cout<<" This is Z0 with same charge NEG"<<endl;
   }
//Xiaoxuan
   if ( z_event.PassedCutZSameChargePosZMassExt(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_Z_SAME_POS_M50);
      cout<<" This is Z0 with same charge POS with Mass Ext"<<endl;
   }

   if ( z_event.PassedCutZSameChargeNegZMassExt(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_Z_SAME_NEG_M50);
      cout<<" This is Z0 with same charge NEG with Mass Ext"<<endl;
   }
//Xiaoxuan

   if ( z_event.PassedCutZRecoilTest(ZBosEvent::sMinZEleCandPtHard) )
   {
      Fill(ev, kCUT_EVENT_RECOTEST);
   }

}


/** */
void ZBosRootFile::Fill(ProtoEvent &ev, ECut cut)
{
   VecBosRootFile::Fill(ev, cut);
}
