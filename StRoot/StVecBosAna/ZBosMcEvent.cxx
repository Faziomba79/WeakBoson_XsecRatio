#include "ZBosMcEvent.h"

#include "StMcEvent/StMcVertex.hh"
#include "StarClassLibrary/StParticleDefinition.hh"

#include "PythiaParticle.h"

ClassImp(ZBosMcEvent)


using namespace std;


ZBosMcEvent::ZBosMcEvent() : VecBosMcEvent(), mP4ZBoson(), mP4Electron(), mP4Positron()
{
}


/** */
TLorentzVector ZBosMcEvent::CalcRecoP4ZBoson()
{
   return mP4Electron + mP4Positron;
}


/** */
void ZBosMcEvent::GetPythiaEvent(StMcEvent &stMcEvent)
{

  // S.F. May 22, 2018 --------------

   StMcVertex *primVertex = stMcEvent.primaryVertex();

   if (!primVertex) {
      Warning("GetPythiaEvent(StMcEvent &stMcEvent)", "Primary vertex not found");
      return;
   }

  //--------------------------------- 

   // Loop over tracks
   std::vector<StMcTrack*, std::allocator<StMcTrack*> >::const_iterator iParticle    = stMcEvent.tracks().begin();
   std::vector<StMcTrack*, std::allocator<StMcTrack*> >::const_iterator lastParticle = stMcEvent.tracks().end();

   for ( ; iParticle!=lastParticle; ++iParticle)
   {
      const StMcTrack *mcTrack = *iParticle;

      //Info("CalcRecoil(StMcEvent &stMcEvent)", "iTrack = %d, mcTrack = %x, partDef = %x", iTrack, mcTrack, mcTrack->particleDefinition());

      if (!mcTrack) {  //S.F. May 22, 2018
         Warning("GetPythiaEvent(StMcEvent &stMcEvent)", "StMcTrack is not valid");
         continue;
      }

      //cout << endl << *mcTrack << endl << endl;

      // Pure MC particles
      TLorentzVector particleP4(mcTrack->fourMomentum().px(), mcTrack->fourMomentum().py(),
         mcTrack->fourMomentum().pz(), mcTrack->fourMomentum().e());

      //if (particleP4.Pt() == 0) {
      //   Warning("CalcRecoil(StMcEvent &stMcEvent)", "MC particle has zero Pt. Skipping it...");
      //   continue;
      //}

      // Consider pure MC particles
      if (mcTrack->key() == 0 && mcTrack->parent())
      {
         int pdgId = mcTrack->pdgId();

         if (pdgId == 23)
            mP4ZBoson   = particleP4;
         else if (pdgId == 11 && mcTrack->parent()->pdgId() == 23)
            mP4Electron   = particleP4;
         else if (pdgId == -11 && mcTrack->parent()->pdgId() == 23)
            mP4Positron   = particleP4;
      }

   }

}
