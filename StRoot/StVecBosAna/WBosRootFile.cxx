#include "WBosRootFile.h"

#include <climits>
#include <sstream>

#include "St_db_Maker/St_db_Maker.h"
#include "StEmcUtil/database/StBemcTables.h"
#include "StEmcUtil/geometry/StEmcGeom.h"

#include "AllAsymHContainer.h"
#include "AsymHContainer.h"
#include "TrackHContainer.h"
#include "EventHContainer.h"
#include "EventDisplayHContainer.h"
#include "WBosEventHContainer.h"
#include "WBosEventHContainerNoEndcap.h"
#include "MCHContainer.h"
#include "WBosEvent.h"
#include "ZBosEvent.h"
#include "VecBosTrack.h"
#include "VecBosVertex.h"

#include "utils/utils.h"

using namespace std;


WBosRootFile::WBosRootFile() : VecBosRootFile()
{
   BookHists();
}


WBosRootFile::WBosRootFile(const char *fname, Option_t *option, Int_t isMc, Bool_t isZ, const char *ftitle, Int_t compress) :
  VecBosRootFile(fname, option, isMc, isZ, ftitle, compress)
{
   gBTowGeom = StEmcGeom::instance("bemc");
   gBTowGeom->printGeom();
   BookHists();
}


WBosRootFile::~WBosRootFile()
{
}


void WBosRootFile::BookHists()
{
   // Delete histograms created in parent class
   if (fHists) { delete fHists; fHists = 0; }
   fHistCuts.clear();

   PlotHelper *ph;

   fHists = new PlotHelper(this);

   //fHists->d["alltracks"] = ph = new TrackHContainer(new TDirectoryFile("alltracks", "alltracks", "", this));
   //fHistCuts[kCUT_EVENT_NOCUT].insert(ph);

   //fHists->d["tracks"] = ph = new TrackHContainer(new TDirectoryFile("tracks", "tracks", "", this));
   //fHistCuts[kCUT_EVENT_NOCUT].insert(ph);

   //fHists->d["tracks.cut_cand"] = ph = new TrackHContainer(new TDirectoryFile("tracks.cut_cand", "tracks.cut_cand", "", this));
   //fHistCuts[kCUT_EVENT_HAS_CANDIDATE_TRACK].insert(ph);

   fHists->d["tracks.cut_w"] = ph = new TrackHContainer(new TDirectoryFile("tracks.cut_w", "tracks.cut_w", "", this));
   fHistCuts[kCUT_EVENT_W].insert(ph);

   fHists->d["tracks.cut_wp"] = ph = new TrackHContainer(new TDirectoryFile("tracks.cut_wp", "tracks.cut_wp", "", this));
   fHistCuts[kCUT_EVENT_W_PLUS].insert(ph);

   fHists->d["tracks.cut_wm"] = ph = new TrackHContainer(new TDirectoryFile("tracks.cut_wm", "tracks.cut_wm", "", this));
   fHistCuts[kCUT_EVENT_W_MINUS].insert(ph);

   fHists->d["event"] = ph = new EventHContainer(new TDirectoryFile("event", "event", "", this));
   fHistCuts[kCUT_EVENT_NOCUT].insert(ph);

   //fHists->d["eventw_nocut"] = ph = new WBosEventHContainer(new TDirectoryFile("eventw_nocut", "eventw_nocut", "", this));
   //fHistCuts[kCUT_EVENT_NOCUT].insert(ph);

   fHists->d["eventw_hpttrack"] = ph = new WBosEventHContainer(new TDirectoryFile("eventw_hpttrack", "eventw_hpttrack", "", this));
   fHistCuts[kCUT_EVENT_HAS_HIGHPT_TRACK].insert(ph);

   fHists->d["event.cut_cand"] = ph = new EventHContainer(new TDirectoryFile("event.cut_cand", "event.cut_cand", "", this));
   fHistCuts[kCUT_EVENT_HAS_CANDIDATE_TRACK].insert(ph);

   fHists->d["event.cut_w"] = ph = new EventHContainer(new TDirectoryFile("event.cut_w", "event.cut_w", "", this));
   fHistCuts[kCUT_EVENT_W].insert(ph);

   fHists->d["event_w"] = ph = new WBosEventHContainer(new TDirectoryFile("event_w", "event_w", "", this));
   fHistCuts[kCUT_EVENT_W].insert(ph);

   //fHists->d["event_display"] = ph = new EventDisplayHContainer(new TDirectoryFile("event_display", "event_display", "", this));
   //fHistCuts[kCUT_EVENT_W].insert(ph);

   fHists->d["event_wp_pt15"] = ph = new WBosEventHContainer(new TDirectoryFile("event_wp_pt15", "event_wp_pt15", "", this));
   fHistCuts[kCUT_POSITIVE_EVENT_PASS_WBOS_PT15].insert(ph);

   fHists->d["event_wm_pt15"] = ph = new WBosEventHContainer(new TDirectoryFile("event_wm_pt15", "event_wm_pt15", "", this));
   fHistCuts[kCUT_NEGATIVE_EVENT_PASS_WBOS_PT15].insert(ph);

   fHists->d["event_wp_noendcap_pt15"] = ph = new WBosEventHContainerNoEndcap(new TDirectoryFile("event_wp_noendcap_pt15", "event_wp_noendcap_pt15", "", this));
   fHistCuts[kCUT_POSITIVE_EVENT_PASS_WBOS_NOENDCAP_PT15].insert(ph);

   fHists->d["event_wm_noendcap_pt15"] = ph = new WBosEventHContainerNoEndcap(new TDirectoryFile("event_wm_noendcap_pt15", "event_wm_noendcap_pt15", "", this));
   fHistCuts[kCUT_NEGATIVE_EVENT_PASS_WBOS_NOENDCAP_PT15].insert(ph);

   fHists->d["event_wp"] = ph = new WBosEventHContainer(new TDirectoryFile("event_wp", "event_wp", "", this));
   fHistCuts[kCUT_EVENT_W_PLUS].insert(ph);

   fHists->d["event_wm"] = ph = new WBosEventHContainer(new TDirectoryFile("event_wm", "event_wm", "", this));
   fHistCuts[kCUT_EVENT_W_MINUS].insert(ph);

   fHists->d["event_wp_noendcap"] = ph = new WBosEventHContainerNoEndcap(new TDirectoryFile("event_wp_noendcap", "event_wp_noendcap", "", this));
   fHistCuts[kCUT_EVENT_W_PLUS_NOENDCAP].insert(ph);

   fHists->d["event_wm_noendcap"] = ph = new WBosEventHContainerNoEndcap(new TDirectoryFile("event_wm_noendcap", "event_wm_noendcap", "", this));
   fHistCuts[kCUT_EVENT_W_MINUS_NOENDCAP].insert(ph);

   if (fIsMc) { // do it only for Monte Carlo
     if (fIsMc == 1 || fIsMc == 2) { // do it only for the W+- Monte Carlo

         fHists->d["event_mc_nocut"] = ph = new MCHContainer(new TDirectoryFile("event_mc_nocut", "event_mc_nocut", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_NOCUT].insert(ph); 

         fHists->d["event_mc_hpttrack"] = ph = new MCHContainer(new TDirectoryFile("event_mc_hpttrack", "event_mc_hpttrack", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_HAS_HIGHPT_TRACK].insert(ph);

         fHists->d["event_hpttrack"] = ph = new EventHContainer(new TDirectoryFile("event_hpttrack", "event_hpttrack", "", this));
         fHistCuts[kCUT_EVENT_HAS_HIGHPT_TRACK].insert(ph);

         fHists->d["event_mc_wp"] = ph = new MCHContainer(new TDirectoryFile("event_mc_wp", "event_mc_wp", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_W_PLUS].insert(ph);
         fHists->d["event_mc_wm"] = ph = new MCHContainer(new TDirectoryFile("event_mc_wm", "event_mc_wm", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_W_MINUS].insert(ph);

         fHists->d["event_mc_wp_noendcap"] = ph = new MCHContainer(new TDirectoryFile("event_mc_wp_noendcap", "event_mc_wp_noendcap", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_W_PLUS_NOENDCAP].insert(ph);
         fHists->d["event_mc_wm_noendcap"] = ph = new MCHContainer(new TDirectoryFile("event_mc_wm_noendcap", "event_mc_wm_noendcap", "", this), fIsMc);
         fHistCuts[kCUT_EVENT_W_MINUS_NOENDCAP].insert(ph);

         fHists->d["event_mc_wp_pt15"] = ph = new MCHContainer(new TDirectoryFile("event_mc_wp_pt15", "event_mc_wp_pt15", "", this), fIsMc);
         fHistCuts[kCUT_POSITIVE_EVENT_PASS_WBOS_PT15].insert(ph);
         fHists->d["event_mc_wm_pt15"] = ph = new MCHContainer(new TDirectoryFile("event_mc_wm_pt15", "event_mc_wm_pt15", "", this), fIsMc);
         fHistCuts[kCUT_NEGATIVE_EVENT_PASS_WBOS_PT15].insert(ph);
        
      }
      fHists->d["event_mc"] = ph = new MCHContainer(new TDirectoryFile("event_mc", "event_mc", "", this), fIsMc);
      fHistCuts[kCUT_EVENT_W].insert(ph);

   } else { // do it only for the data

      fHists->d["event_display"] = ph = new EventDisplayHContainer(new TDirectoryFile("event_display", "event_display", "", this));
      fHistCuts[kCUT_EVENT_W].insert(ph);

      fHists->d["event_qcd_positive_pt15"] = ph = new WBosEventHContainer(new TDirectoryFile("event_qcd_positive_pt15", "event_qcd_positive_pt15", "", this));
      fHistCuts[kCUT_POSITIVE_EVENT_PASS_QCD_PT15].insert(ph);
      fHists->d["event_qcd_negative_pt15"] = ph = new WBosEventHContainer(new TDirectoryFile("event_qcd_negative_pt15", "event_qcd_negative_pt15", "", this));
      fHistCuts[kCUT_NEGATIVE_EVENT_PASS_QCD_PT15].insert(ph);

      fHists->d["event_qcd_positive"] = ph = new WBosEventHContainer(new TDirectoryFile("event_qcd_positive", "event_qcd_positive", "", this));
      fHistCuts[kCUT_POSITIVE_EVENT_PASS_QCD].insert(ph);
      fHists->d["event_qcd_negative"] = ph = new WBosEventHContainer(new TDirectoryFile("event_qcd_negative", "event_qcd_negative", "", this));
      fHistCuts[kCUT_NEGATIVE_EVENT_PASS_QCD].insert(ph);

      fHists->d["asym"] = ph = new AllAsymHContainer(new TDirectoryFile("asym", "asym", "", this));
      fHistCuts[kCUT_EVENT_W].insert(ph);
   }

   this->cd();
}


/** */
void WBosRootFile::Fill(ProtoEvent &ev)
{
   WBosEvent& w_event = (WBosEvent&) ev;

   Fill(ev, kCUT_EVENT_NOCUT);

   if ( w_event.HasCandidateEle() )
      Fill(ev, kCUT_EVENT_HAS_CANDIDATE_TRACK);

   if (  w_event.HasCandidateEle() && w_event.HasHighPtTrack() )
      Fill(ev, kCUT_EVENT_HAS_HIGHPT_TRACK);

   if ( w_event.PassedCutWBos(WBosEvent::sMinElectronPtHard) )
   {
      Fill(ev, kCUT_EVENT_W);

      if ( w_event.PassedCutWBosPlus(WBosEvent::sMinElectronPtHard) ) 
         Fill(ev, kCUT_EVENT_W_PLUS);

      if ( w_event.PassedCutWBosMinus(WBosEvent::sMinElectronPtHard) )
         Fill(ev, kCUT_EVENT_W_MINUS);
   }

   if ( w_event.PassedCutWBos(WBosEvent::sMinElectronPtLight) )
   {
      if ( w_event.PassedCutWBosPlus(WBosEvent::sMinElectronPtLight) ) 
         Fill(ev, kCUT_POSITIVE_EVENT_PASS_WBOS_PT15);

      if ( w_event.PassedCutWBosMinus(WBosEvent::sMinElectronPtLight) )
         Fill(ev, kCUT_NEGATIVE_EVENT_PASS_WBOS_PT15);
   }

   if ( w_event.PassedCutWBosNoEndcap(WBosEvent::sMinElectronPtHard) )
   {
      Fill(ev, kCUT_EVENT_W_NOENDCAP);

      if ( w_event.PassedCutWBosPlusNoEndcap(WBosEvent::sMinElectronPtHard) ) 
         Fill(ev, kCUT_EVENT_W_PLUS_NOENDCAP);

      if ( w_event.PassedCutWBosMinusNoEndcap(WBosEvent::sMinElectronPtHard) )
         Fill(ev, kCUT_EVENT_W_MINUS_NOENDCAP);
   }

   if ( w_event.PassedCutWBosNoEndcap(WBosEvent::sMinElectronPtLight) )
   {
      if ( w_event.PassedCutWBosPlusNoEndcap(WBosEvent::sMinElectronPtLight) ) 
         Fill(ev, kCUT_POSITIVE_EVENT_PASS_WBOS_NOENDCAP_PT15);

      if ( w_event.PassedCutWBosMinusNoEndcap(WBosEvent::sMinElectronPtLight) )
         Fill(ev, kCUT_NEGATIVE_EVENT_PASS_WBOS_NOENDCAP_PT15);
   }
   
   // Select QCD with Pt>16
   if ( w_event.PassedCutQcdBkg(WBosEvent::sMinElectronPtLight) )
      Fill(ev, kCUT_EVENT_PASS_QCD_PT15);

   if ( w_event.PassedCutQcdBkgPlus(WBosEvent::sMinElectronPtLight) )
      Fill(ev, kCUT_POSITIVE_EVENT_PASS_QCD_PT15);

   if ( w_event.PassedCutQcdBkgMinus(WBosEvent::sMinElectronPtLight) )
      Fill(ev, kCUT_NEGATIVE_EVENT_PASS_QCD_PT15);

   // Select QCD with Pt>25
   if ( w_event.PassedCutQcdBkg(WBosEvent::sMinElectronPtHard) )
      Fill(ev, kCUT_EVENT_PASS_QCD);

   if ( w_event.PassedCutQcdBkgPlus(WBosEvent::sMinElectronPtHard) )
      Fill(ev, kCUT_POSITIVE_EVENT_PASS_QCD);

   if ( w_event.PassedCutQcdBkgMinus(WBosEvent::sMinElectronPtHard) )
      Fill(ev, kCUT_NEGATIVE_EVENT_PASS_QCD);
   
}


/** */
void WBosRootFile::Fill(ProtoEvent &ev, ECut cut)
{
   VecBosRootFile::Fill(ev, cut);
}
