/*****************************************************************************
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef Z0_MCHContainer_h
#define Z0_MCHContainer_h

#include "TDirectoryFile.h"

#include "utils/PlotHelper.h"
#include "utils/ProtoEvent.h"

#include "VecBosTrack.h"


/**
 *
 */
class Z0_MCHContainer : public PlotHelper
{
private:

   //TH1* fhPseudoMass_ch[N_SILICON_CHANNELS];


public:

   Z0_MCHContainer();
   Z0_MCHContainer(TDirectory *dir, Int_t);

   void Fill(ProtoEvent &ev);
   //void PostFill();
   //void PostFill(PlotHelper &oc) {}

private:

   void BookHists();
   Int_t mMcType;
   //static const int PtBins = 7;
   static const int PtBins = 6;
   Double_t xBinsPt[PtBins+1];

   static const int RapBins = 3;
   Double_t xBinsRap[RapBins+1];
   Double_t xBinsRapPR[RapBins+1];
   Double_t xBinsRapMR[RapBins+1];
   Double_t BinsPt_XSec[12];

   ClassDef(Z0_MCHContainer, 1)
};

#endif
