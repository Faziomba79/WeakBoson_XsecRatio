#include "Z0_MCHContainer.h"

#include "TNtuple.h"
#include "TF1.h"
#include "TF2.h"
#include "TProfile.h"
#include "TText.h"

#include "ZBosEvent.h"
#include "ZBosMcEvent.h"
#include "Globals.h"

#include "utils/H1I.h"
#include "utils/H1F.h"
#include "utils/H1D.h"
#include "utils/H2D.h"
#include "utils/H2I.h"
#include "utils/H2F.h"

ClassImp(Z0_MCHContainer)

using namespace std;


/** Default constructor. */
Z0_MCHContainer::Z0_MCHContainer() : PlotHelper()
{
   BookHists();
}


Z0_MCHContainer::Z0_MCHContainer(TDirectory *dir, Int_t McType) : PlotHelper(dir)
{
   mMcType = McType;
   BookHists();
}


/** */
void Z0_MCHContainer::BookHists()
{
   string shName;
   TH1*   hist;

   fDir->cd();

   //BinsPt_XSec[12]  = {0, 1.25, 2.5, 3.75, 5, 7.5, 10, 12.5, 15, 17.5, 20, 25};
   BinsPt_XSec[0]  = 0;
   BinsPt_XSec[1]  = 1.25;
   BinsPt_XSec[2]  = 2.5;
   BinsPt_XSec[3]  = 3.75;
   BinsPt_XSec[4]  = 5;
   BinsPt_XSec[5]  = 7.5;
   BinsPt_XSec[6]  = 10;
   BinsPt_XSec[7]  = 12.5;
   BinsPt_XSec[8]  = 15;
   BinsPt_XSec[9]  = 17.5;
   BinsPt_XSec[10] = 20;
   BinsPt_XSec[11] = 25;

   o["hRecoVsGen_ZBosonRap"] = new rh::H2D("hRecoVsGen_ZBosonRap", "; Reco. Z^{0} rapidity y; Gen. Z^{0} rapidity y", 20, -2, 2, 20, -2, 2, "colz LOGZ");
   o["hRecoVsGen_ZBosonRap_Pt25_Minv"] = new rh::H2D("hRecoVsGen_ZBosonRap_Pt25_Minv", "; Reco. Z^{0} rapidity y; Gen. Z^{0} rapidity y", 20, -2, 2, 20, -2, 2, "colz LOGZ");
   o["hRecoVsGen_ZBosonRap_Eta1_Pt25_Minv"] = new rh::H2D("hRecoVsGen_ZBosonRap_Eta1_Pt25_Minv", "; Reco. Z^{0} rapidity y; Gen. Z^{0} rapidity y", 20, -2, 2, 20, -2, 2, "colz LOGZ");
   o["hRecoVsGen_ZBosonPz"]  = new rh::H2D("hRecoVsGen_ZBosonPz", "; Reco. P^{Z^{0}}_{z} [GeV/c];  Gen. P^{Z^{0}}_{z} [GeV/c]", 30, -60, 60, 30, -60, 60, "colz LOGZ");
   o["hRecoVsGen_ZBosonPt"]  = new rh::H2D("hRecoVsGen_ZBosonPt", "; Reco. P^{Z^{0}}_{T} [GeV/c]; Gen. P^{Z^{0}}_{T} [GeV/c]", 20, 0, 25, 20, 0, 25, "colz LOGZ");
   o["hRecoVsGen_ZBosonPt_Eta1_Minv"]  = new rh::H2D("hRecoVsGen_ZBosonPt_Eta1_Minv", "; Reco. P^{Z^{0}}_{T} [GeV/c]; Gen. P^{Z^{0}}_{T} [GeV/c]", 20, 0, 25, 20, 0, 25, "colz LOGZ");
   o["hRecoVsGen_ZBosonPt_Eta1_Pt25_Minv"]  = new rh::H2D("hRecoVsGen_ZBosonPt_Eta1_Pt25_Minv", "; Reco. P^{Z^{0}}_{T} [GeV/c]; Gen. P^{Z^{0}}_{T} [GeV/c]", 20, 0, 25, 20, 0, 25, "colz LOGZ");
   o["hRecoVsGen_ZBosonPt_XSecBins"]  = new rh::H2D("hRecoVsGen_ZBosonPt_XSecBins", "; Reco. P^{Z^{0}}_{T} [GeV/c]; Gen. P^{Z^{0}}_{T} [GeV/c]", 11, BinsPt_XSec, 11, BinsPt_XSec, "colz LOGZ");
   o["hRecoVsGen_ZBosonPt_Eta1_Minv_XSecBins"]  = new rh::H2D("hRecoVsGen_ZBosonPt_Eta1_Minv_XSecBins", "; Reco. P^{Z^{0}}_{T} [GeV/c]; Gen. P^{Z^{0}}_{T} [GeV/c]", 11, BinsPt_XSec, 11, BinsPt_XSec, "colz LOGZ");
   o["hRecoVsGen_ZBosonPt_Eta1_Pt25_Minv_XSecBins"]  = new rh::H2D("hRecoVsGen_ZBosonPt_Eta1_Pt25_Minv_XSecBins", "; Reco. P^{Z^{0}}_{T} [GeV/c]; Gen. P^{Z^{0}}_{T} [GeV/c]", 11, BinsPt_XSec, 11, BinsPt_XSec, "colz LOGZ");

   o["hRecoVsGen_ElectronE"]   = new rh::H2D("hRecoVsGen_ElectronE", "; Reco. E^{e-} [GeV/c]; Gen. E^{e-} [GeV/c]", 60, 20, 80, 60, 20, 80, "colz LOGZ");
   o["hRecoVsGen_ElectronPz"]  = new rh::H2D("hRecoVsGen_ElectronPz", "; Reco. P^{e-}_{z} [GeV/c]; Gen. P^{e-}_{z} [GeV/c]", 40, -60, 60, 40, -60, 60, "colz LOGZ");
   o["hRecoVsGen_ElectronPt"]  = new rh::H2D("hRecoVsGen_ElectronPt", "; Reco. P^{e-}_{T} [GeV/c]; Gen. P^{e-}_{T} [GeV/c]", 40, 0, 60, 40, 0, 60, "colz LOGZ");

   o["hRecoVsGen_PositronE"]   = new rh::H2D("hRecoVsGen_PositronE", "; Reco. E^{e+} [GeV/c]; Gen. E^{e+} [GeV/c]", 60, 20, 80, 60, 20, 80, "colz LOGZ");
   o["hRecoVsGen_PositronPz"]  = new rh::H2D("hRecoVsGen_PositronPz", "; Reco. P^{e+}_{z} [GeV/c]; Gen. P^{e+}_{z} [GeV/c]", 40, -60, 60, 40, -60, 60, "colz LOGZ");
   o["hRecoVsGen_PositronPt"]  = new rh::H2D("hRecoVsGen_PositronPt", "; Reco. P^{e+}_{T} [GeV/c]; Gen. P^{e+}_{T} [GeV/c]", 40, 0, 60, 40, 0, 60, "colz LOGZ");

   o["hGen_ElectronVsPositron_Pt"] = new rh::H2D("hGen_ElectronVsPositron_Pt", "; Gen. P^{e+}_{T} [GeV/c]; Gen. P^{e-}_{T} [GeV/c]", 40, 0, 60, 40, 0, 60, "colz LOGZ");
   o["hGen_ElectronVsZBoson_Pt"]   = new rh::H2D("hGen_ElectronVsZBoson_Pt", "; Gen. P^{Z^{0}}_{T} [GeV/c]; Gen. P^{e-}_{T} [GeV/c]", 40, 20, 60, 40, 20, 60, "colz LOGZ");
   o["hGen_ElectronEtaVsZBosonRap"]  = new rh::H2D("hGen_ElectronEtaVsZBosonRap", "; Gen. #y^{Z^{0}}; Gen. #eta^{e-}", 40, -1.5, 1.5, 40, -1.5, 1.5, "colz LOGZ");

   o["hZBosonE"]   = new rh::H1F("hZBosonE", "; Z^{0} energy [GeV]; Events", 100, 20., 200., "hist GRIDX");
   o["hZBosonPz"]  = new rh::H1F("hZBosonPz", "; P^{Z^{0}}_{z} [GeV/c]; Events", 30, -60., 60., "hist GRIDX");
   o["hZBosonPt"]  = new rh::H1F("hZBosonPt", "; P^{Z^{0}}_{T} [GeV/c]; Events", 20, 0., 25., "hist GRIDX");
   o["hZBosonPt_40GeV"]  = new rh::H1F("hZBosonPt_40GeV", "; P^{Z^{0}}_{T} [GeV/c]; Events", 40, 0., 40., "hist GRIDX");
   o["hZBosonPt_zoomin"]  = new rh::H1F("hZBosonPt_zoomin", "; P^{Z^{0}}_{T} [GeV/c]; Events", 20, 0., 10., "hist GRIDX");
   o["hZBosonPt_Eta1_Minv"]  = new rh::H1F("hZBosonPt_Eta1_Minv", "; P^{Z^{0}}_{T} [GeV/c]; Events", 20, 0., 25., "hist GRIDX"); // for resolution studies
   o["hZBosonPt_Eta1_Pt25_Minv"]  = new rh::H1F("hZBosonPt_Eta1_Pt25_Minv", "; P^{Z^{0}}_{T} [GeV/c]; Events", 20, 0., 25., "hist GRIDX"); // for resolution studies
   o["hZBosonPt_XSecBins"]  = new rh::H1D("hZBosonPt_XSecBins", "; P^{Z^{0}}_{T} [GeV/c]; Events", 11, BinsPt_XSec, "hist GRIDX");
   o["hZBosonPt_Eta1_Minv_XSecBins"]  = new rh::H1D("hZBosonPt_Eta1_Minv_XSecBins", "; P^{Z^{0}}_{T} [GeV/c]; Events", 11, BinsPt_XSec, "hist GRIDX"); // for resolution studies
   o["hZBosonPt_Eta1_Pt25_Minv_XSecBins"]  = new rh::H1D("hZBosonPt_Eta1_Pt25_Minv_XSecBins", "; P^{Z^{0}}_{T} [GeV/c]; Events", 11, BinsPt_XSec, "hist GRIDX"); // for resolution studies
   o["hZBosonEta"] = new rh::H1F("hZBosonEta", "; Z^{0} pseudo-rapidity #eta; Events", 20, -4, 4, "hist GRIDX");
   o["hZBosonRap"] = new rh::H1F("hZBosonRap", "; Z^{0} rapidity y; Events", 20, -2, 2, "hist GRIDX");
   o["hZBosonRap_Pt25_Minv"] = new rh::H1F("hZBosonRap_Pt25_Minv", "; Z^{0} rapidity y; Events", 20, -2, 2, "hist GRIDX"); // for resolution studies
   o["hZBosonRap_Eta1_Pt25_Minv"] = new rh::H1F("hZBosonRap_Eta1_Pt25_Minv", "; Z^{0} rapidity y; Events", 20, -2, 2, "hist GRIDX"); // for resolution studies

   o["hElectronE"]   = new rh::H1F("hElectronE", "; e- energy [GeV]; Events", 60, 20., 80., "hist GRIDX");
   o["hElectronPz"]  = new rh::H1F("hElectronPz", "; P^{e-}_{z} [GeV/c]; Events", 40, -60., 60., "hist GRIDX");
   o["hElectronPt"]  = new rh::H1F("hElectronPt", "; P^{e-}_{T} [GeV/c]; Events", 40, 0., 60., "hist GRIDX");
   o["hElectronEta"] = new rh::H1F("hElectronEta", "; e- pseudo-rapidity #eta; Events", 20, -4, 4, "hist GRIDX");
   o["hElectronRap"] = new rh::H1F("hElectronRap", "; e- rapidity y; Events", 20, -2, 2, "hist GRIDX");

   o["hPositronE"]   = new rh::H1F("hPositronE", "; e+ energy [GeV]; Events", 60, 20., 80., "hist GRIDX");
   o["hPositronPz"]  = new rh::H1F("hPositronPz", "; P^{e+}_{z} [GeV/c]; Events", 40, -60., 60., "hist GRIDX");
   o["hPositronPt"]  = new rh::H1F("hPositronPt", "; P^{e+}_{T} [GeV/c]; Events", 40, 0., 60., "hist GRIDX");
   o["hPositronEta"] = new rh::H1F("hPositronEta", "; e+ pseudo-rapidity #eta; Events", 20, -4, 4, "hist GRIDX");
   o["hPositronRap"] = new rh::H1F("hPositronRap", "; e+ rapidity y; Events", 20, -2, 2, "hist GRIDX");

   /*
   // Resoulution histograms
   
   char hname[21][50];
   char hname2[21][50];  
   Int_t Nbin = (TH1*) o["hZBosonRap"]) -> GetNbinsX();
   cout << "Test Nbins = " << Nbin << endl;
   for (int j=1; j< Nbins; j++) {
       sprintf(hname[j],"hResolutiont_ZBosonRap_bin%d",j); 
       sprintf(hname2[j],"bint %d; (Gen-Rec)/Gen ZBoson y; Events",j);
       o[hname[j]]  = hist = new TH1D(hname[j], hname2[j], 80, -5, 5.);
   }
   */  

  
   o["hResolution_ZBosonPt_bin1"]  = hist = new TH1D("hResolution_ZBosonPt_bin1", "bin 1; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin2"]  = hist = new TH1D("hResolution_ZBosonPt_bin2", "bin 2; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin3"]  = hist = new TH1D("hResolution_ZBosonPt_bin3", "bin 3; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin4"]  = hist = new TH1D("hResolution_ZBosonPt_bin4", "bin 4; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin5"]  = hist = new TH1D("hResolution_ZBosonPt_bin5", "bin 5; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin6"]  = hist = new TH1D("hResolution_ZBosonPt_bin6", "bin 6; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin7"]  = hist = new TH1D("hResolution_ZBosonPt_bin7", "bin 7; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin8"]  = hist = new TH1D("hResolution_ZBosonPt_bin8", "bin 8; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin9"]  = hist = new TH1D("hResolution_ZBosonPt_bin9", "bin 9; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_bin10"] = hist = new TH1D("hResolution_ZBosonPt_bin10", "bin 10; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);

   o["hResolution_ZBosonPt_XSecBins1"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins1", "XSecBin 1; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins2"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins2", "XSecBin 2; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins3"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins3", "XSecBin 3; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins4"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins4", "XSecBin 4; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins5"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins5", "XSecBin 5; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins6"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins6", "XSecBin 6; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins7"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins7", "XSecBin 7; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins8"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins8", "XSecBin 8; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins9"]  = hist = new TH1D("hResolution_ZBosonPt_XSecBins9", "XSecBin 9; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins10"] = hist = new TH1D("hResolution_ZBosonPt_XSecBins10", "XSecBin 10; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonPt_XSecBins11"] = hist = new TH1D("hResolution_ZBosonPt_XSecBins11", "XSecBin 11; (Gen-Rec)/Gen ZBoson P^{Z^{0}}_{T} Events", 80, -0.5, 0.5);

   o["hAbsoResolution_ZBosonPt_bin1"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin1", "bin 1; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin2"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin2", "bin 2; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin3"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin3", "bin 3; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin4"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin4", "bin 4; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin5"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin5", "bin 5; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin6"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin6", "bin 6; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin7"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin7", "bin 7; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin8"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin8", "bin 8; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin9"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_bin9", "bin 9; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_bin10"] = hist = new TH1D("hAbsoResolution_ZBosonPt_bin10", "bin 10; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);

   o["hAbsoResolution_ZBosonPt_XSecBins1"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins1", "XSecBin 1; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins2"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins2", "XSecBin 2; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins3"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins3", "XSecBin 3; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins4"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins4", "XSecBin 4; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins5"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins5", "XSecBin 5; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins6"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins6", "XSecBin 6; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins7"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins7", "XSecBin 7; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins8"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins8", "XSecBin 8; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins9"]  = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins9", "XSecBin 9; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins10"] = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins10", "XSecBin 10; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);
   o["hAbsoResolution_ZBosonPt_XSecBins11"] = hist = new TH1D("hAbsoResolution_ZBosonPt_XSecBins11", "XSecBin 11; Gen-Rec ZBoson P^{Z^{0}}_{T} [GeV/c] Events", 80, -5, 5);

   o["hAbsoResolution_ElectronCandPt_25_pt_35"]  = hist = new TH1D("hAbsoResolution_ElectronCandPt_25_pt_35", "25 < p^{e-}_{T} < 35 GeV^{2}; Gen-Rec p^{e-}_{T} [GeV/c]", 40, -5, 5);
   o["hAbsoResolution_PositronCandPt_25_pt_35"]  = hist = new TH1D("hAbsoResolution_PositronCandPt_25_pt_35", "25 < p^{e+}_{T} < 35 GeV^{2}; Gen-Rec p^{e+}_{T} [GeV/c]", 40, -5, 5);
   o["hAbsoResolution_ElectronCandPt_35_pt_45"]  = hist = new TH1D("hAbsoResolution_ElectronCandPt_35_pt_45", "35 < p^{e-}_{T} < 45 GeV^{2}; Gen-Rec p^{e-}_{T} [GeV/c]", 40, -5, 5);
   o["hAbsoResolution_PositronCandPt_35_pt_45"]  = hist = new TH1D("hAbsoResolution_PositronCandPt_35_pt_45", "35 < p^{e+}_{T} < 45 GeV^{2}; Gen-Rec p^{e+}_{T} [GeV/c]", 40, -5, 5);
   o["hAbsoResolution_ElectronCandPt_45_pt_55"]  = hist = new TH1D("hAbsoResolution_ElectronCandPt_45_pt_55", "45 < p^{e-}_{T} < 55 GeV^{2}; Gen-Rec p^{e-}_{T} [GeV/c]", 40, -5, 5);
   o["hAbsoResolution_PositronCandPt_45_pt_55"]  = hist = new TH1D("hAbsoResolution_PositronCandPt_45_pt_55", "45 < p^{e+}_{T} < 55 GeV^{2}; Gen-Rec p^{e+}_{T} [GeV/c]", 40, -5, 5);

   o["hResolution_ZBosonRap_bin1"]  = hist = new TH1D("hResolution_ZBosonRap_bin1", "bin 1; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin2"]  = hist = new TH1D("hResolution_ZBosonRap_bin2", "bin 2; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin3"]  = hist = new TH1D("hResolution_ZBosonRap_bin3", "bin 3; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin4"]  = hist = new TH1D("hResolution_ZBosonRap_bin4", "bin 4; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin5"]  = hist = new TH1D("hResolution_ZBosonRap_bin5", "bin 5; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin6"]  = hist = new TH1D("hResolution_ZBosonRap_bin6", "bin 6; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin7"]  = hist = new TH1D("hResolution_ZBosonRap_bin7", "bin 7; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin8"]  = hist = new TH1D("hResolution_ZBosonRap_bin8", "bin 8; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin9"]  = hist = new TH1D("hResolution_ZBosonRap_bin9", "bin 9; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   o["hResolution_ZBosonRap_bin10"] = hist = new TH1D("hResolution_ZBosonRap_bin10", "bin 10; (Gen-Rec)/Gen ZBoson y; Events", 80, -0.5, 0.5);
   
   o["hAbsoResolution_ZBosonRap_bin1"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin1", "bin 1; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin2"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin2", "bin 2; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin3"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin3", "bin 3; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin4"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin4", "bin 4; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin5"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin5", "bin 5; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin6"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin6", "bin 6; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin7"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin7", "bin 7; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin8"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin8", "bin 8; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin9"]  = hist = new TH1D("hAbsoResolution_ZBosonRap_bin9", "bin 9; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);
   o["hAbsoResolution_ZBosonRap_bin10"] = hist = new TH1D("hAbsoResolution_ZBosonRap_bin10", "bin 10; Gen-Rec ZBoson y; Events", 80, -0.1, 0.1);


   // For testing the reconstruction algorithm in the W boson case
   o["hTrackRecoilTpcNeutralsPt_GenOverReco"] = new rh::H2D("hTrackRecoilTpcNeutralsPt_GenOverReco", ";Track-based Recoil P_{T}; Correction factor",40, 0, 40, 50, 0, 20, "colz LOGZ");
   o["hTrackRecoilTpcNeutralsPt_GenOverReco_zoomin"] = new rh::H2D("hTrackRecoilTpcNeutralsPt_GenOverReco_zoomin", "; Track-based Recoil P_{T}; Correction factor",20, 0, 10, 50, 0, 20, "colz LOGZ");
   
}


/**
 * Fill histograms using information about the Monte-Carlo event.
 */
void Z0_MCHContainer::Fill(ProtoEvent &ev)
{
    ZBosEvent& event = (ZBosEvent&) ev;                    // MC reconstructed
    ZBosMcEvent* mcEvent = (ZBosMcEvent*) event.mMcEvent;  // MC generated


// Calculate the resolution in each of the y bins we use for the xsec 
    Double_t ZRap = mcEvent->mP4ZBoson.Rapidity();

    if (ZRap >= -1.0 && ZRap < -0.8) {
       ((TH1*) o["hResolution_ZBosonRap_bin1"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin1"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= -0.8 && ZRap < -0.6) {
       ((TH1*) o["hResolution_ZBosonRap_bin2"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin2"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= -0.6 && ZRap < -0.4) {
       ((TH1*) o["hResolution_ZBosonRap_bin3"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin3"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= -0.4 && ZRap < -0.2) {
       ((TH1*) o["hResolution_ZBosonRap_bin4"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin4"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= -0.2 && ZRap < 0.0) {
       ((TH1*) o["hResolution_ZBosonRap_bin5"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin5"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= 0.0 && ZRap < 0.2) {
       ((TH1*) o["hResolution_ZBosonRap_bin6"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin6"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= 0.2 && ZRap < 0.4) {
       ((TH1*) o["hResolution_ZBosonRap_bin7"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin7"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= 0.4 && ZRap < 0.6) {
       ((TH1*) o["hResolution_ZBosonRap_bin8"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin8"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= 0.6 && ZRap < 0.8) {
       ((TH1*) o["hResolution_ZBosonRap_bin9"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin9"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    } else if (ZRap >= 0.8 && ZRap < 1.0) {
       ((TH1*) o["hResolution_ZBosonRap_bin10"])     ->Fill((mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity())/mcEvent->mP4ZBoson.Rapidity());
       ((TH1*) o["hAbsoResolution_ZBosonRap_bin10"]) ->Fill(mcEvent->mP4ZBoson.Rapidity()-event.GetVecBosonP4().Rapidity());
    }

// Calculate the resolution in each of the p_T bins we use for the xsec 
    Double_t ZPt = mcEvent->mP4ZBoson.Pt();

    if (ZPt >= 0. && ZPt < 2.5) {
       ((TH1*) o["hResolution_ZBosonPt_bin1"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin1"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 2.5 && ZPt < 5.0) {
       ((TH1*) o["hResolution_ZBosonPt_bin2"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin2"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 5.0 && ZPt < 7.5) {
       ((TH1*) o["hResolution_ZBosonPt_bin3"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin3"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 7.5 && ZPt < 10.) {
       ((TH1*) o["hResolution_ZBosonPt_bin4"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin4"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 10. && ZPt < 12.5) {
       ((TH1*) o["hResolution_ZBosonPt_bin5"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin5"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 12.5 && ZPt < 15.) {
       ((TH1*) o["hResolution_ZBosonPt_bin6"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin6"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 15. && ZPt < 17.5) {
       ((TH1*) o["hResolution_ZBosonPt_bin7"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin7"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 17.5 && ZPt < 20.) {
       ((TH1*) o["hResolution_ZBosonPt_bin8"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin8"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 20. && ZPt < 22.5) {
       ((TH1*) o["hResolution_ZBosonPt_bin9"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin9"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= 22.5 && ZPt < 25.) {
       ((TH1*) o["hResolution_ZBosonPt_bin10"])    ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_bin10"])->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    }

    if (ZPt >= BinsPt_XSec[0] && ZPt < BinsPt_XSec[1]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins1"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins1"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[1] && ZPt < BinsPt_XSec[2]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins2"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins2"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[2] && ZPt < BinsPt_XSec[3]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins3"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins3"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[3] && ZPt < BinsPt_XSec[4]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins4"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins4"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[4] && ZPt < BinsPt_XSec[5]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins5"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins5"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[5] && ZPt < BinsPt_XSec[6]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins6"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins6"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[6] && ZPt < BinsPt_XSec[7]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins7"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins7"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[7] && ZPt < BinsPt_XSec[8]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins8"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins8"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[8] && ZPt < BinsPt_XSec[9]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins9"])     ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins9"]) ->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[9] && ZPt < BinsPt_XSec[10]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins10"])    ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins10"])->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    } else if (ZPt >= BinsPt_XSec[10] && ZPt < BinsPt_XSec[11]) {
       ((TH1*) o["hResolution_ZBosonPt_XSecBins11"])    ->Fill((mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt())/mcEvent->mP4ZBoson.Pt());
       ((TH1*) o["hAbsoResolution_ZBosonPt_XSecBins11"])->Fill(mcEvent->mP4ZBoson.Pt()-event.GetVecBosonP4().Pt());
    }


    Double_t ElePt = mcEvent->mP4Electron.Pt();
    if ( ElePt > 25 && ElePt < 35 ) {
      ((TH1*) o["hAbsoResolution_ElectronCandPt_25_pt_35"]) ->Fill(mcEvent->mP4Electron.Pt()-event.GetElectronCand_P4().Pt());
      ((TH1*) o["hAbsoResolution_PositronCandPt_25_pt_35"]) ->Fill(mcEvent->mP4Positron.Pt()-event.GetPositronCand_P4().Pt());
    } else if ( ElePt > 35 && ElePt < 45) {
      ((TH1*) o["hAbsoResolution_ElectronCandPt_35_pt_45"]) ->Fill(mcEvent->mP4Electron.Pt()-event.GetElectronCand_P4().Pt());
      ((TH1*) o["hAbsoResolution_PositronCandPt_35_pt_45"]) ->Fill(mcEvent->mP4Positron.Pt()-event.GetPositronCand_P4().Pt());
    } else if ( ElePt > 45 && ElePt < 55) {
      ((TH1*) o["hAbsoResolution_ElectronCandPt_45_pt_55"]) ->Fill(mcEvent->mP4Electron.Pt()-event.GetElectronCand_P4().Pt());
      ((TH1*) o["hAbsoResolution_PositronCandPt_45_pt_55"]) ->Fill(mcEvent->mP4Positron.Pt()-event.GetPositronCand_P4().Pt());
    }

//------------------------------------------------------------------

   ((TH1*) o["hElectronE"])      -> Fill(mcEvent->mP4Electron.E());
   ((TH1*) o["hElectronPz"])     -> Fill(mcEvent->mP4Electron.Pz());
   ((TH1*) o["hElectronPt"])     -> Fill(mcEvent->mP4Electron.Pt());
   ((TH1*) o["hElectronEta"])    -> Fill(mcEvent->mP4Electron.Eta());
   ((TH1*) o["hElectronRap"])    -> Fill(mcEvent->mP4Electron.Rapidity());

   ((TH1*) o["hPositronE"])      -> Fill(mcEvent->mP4Positron.E());
   ((TH1*) o["hPositronPz"])     -> Fill(mcEvent->mP4Positron.Pz());
   ((TH1*) o["hPositronPt"])     -> Fill(mcEvent->mP4Positron.Pt());
   ((TH1*) o["hPositronEta"])    -> Fill(mcEvent->mP4Positron.Eta());
   ((TH1*) o["hPositronRap"])    -> Fill(mcEvent->mP4Positron.Rapidity());

   if (mcEvent->mP4ZBoson.Mag() > 0) {
     //((TH1*) o["hZBosonE"])      -> Fill(mcEvent->mP4ZBoson.E());
     ((TH1*) o["hZBosonPz"])          -> Fill(mcEvent->mP4ZBoson.Pz());
     ((TH1*) o["hZBosonPt"])          -> Fill(mcEvent->mP4ZBoson.Pt());
     ((TH1*) o["hZBosonPt_40GeV"])    -> Fill(mcEvent->mP4ZBoson.Pt());
     ((TH1*) o["hZBosonPt_zoomin"])   -> Fill(mcEvent->mP4ZBoson.Pt());
     ((TH1*) o["hZBosonPt_XSecBins"]) -> Fill(mcEvent->mP4ZBoson.Pt());
     ((TH1*) o["hZBosonEta"])         -> Fill(mcEvent->mP4ZBoson.Eta());
     ((TH1*) o["hZBosonRap"])         -> Fill(mcEvent->mP4ZBoson.Rapidity());
   }

   ((TH2*) o["hRecoVsGen_ZBosonRap"])  -> Fill(event.GetVecBosonP4().Rapidity(), mcEvent->mP4ZBoson.Rapidity());
   ((TH2*) o["hRecoVsGen_ZBosonPz"])   -> Fill(event.GetVecBosonP4().Pz(), mcEvent->mP4ZBoson.Pz());
   ((TH2*) o["hRecoVsGen_ZBosonPt"])   -> Fill(event.GetVecBosonP4().Pt(), mcEvent->mP4ZBoson.Pt());
   ((TH2*) o["hRecoVsGen_ZBosonPt_XSecBins"]) -> Fill(event.GetVecBosonP4().Pt(), mcEvent->mP4ZBoson.Pt());


   if (mcEvent->mP4ZBoson.M() > 73 &&  mcEvent->mP4ZBoson.M() < 114 ) { // this is for efficiency studies

     if (mcEvent->mP4Positron.Pt()>25 && mcEvent->mP4Electron.Pt()>25) { 
        ((TH2*) o["hRecoVsGen_ZBosonRap_Pt25_Minv"] )  -> Fill(event.GetVecBosonP4().Rapidity(), mcEvent->mP4ZBoson.Rapidity());
        ((TH1*) o["hZBosonRap_Pt25_Minv"] )            -> Fill(mcEvent->mP4ZBoson.Rapidity());
     }

     if (fabs(mcEvent->mP4Positron.Eta())<1.2 && fabs(mcEvent->mP4Electron.Eta()<1.2)) { 
        ((TH2*) o["hRecoVsGen_ZBosonPt_Eta1_Minv"] )          -> Fill(event.GetVecBosonP4().Pt(), mcEvent->mP4ZBoson.Pt());
        ((TH2*) o["hRecoVsGen_ZBosonPt_Eta1_Minv_XSecBins"] ) -> Fill(event.GetVecBosonP4().Pt(), mcEvent->mP4ZBoson.Pt());
        ((TH1*) o["hZBosonPt_Eta1_Minv"] )                    -> Fill(mcEvent->mP4ZBoson.Pt());
        ((TH1*) o["hZBosonPt_Eta1_Minv_XSecBins"] )           -> Fill(mcEvent->mP4ZBoson.Pt());
     }

     if (mcEvent->mP4Positron.Pt()>25 && mcEvent->mP4Electron.Pt()>25 &&
         fabs(mcEvent->mP4Positron.Eta())<1.2 && fabs(mcEvent->mP4Electron.Eta()<1.2)) { 
        ((TH2*) o["hRecoVsGen_ZBosonRap_Eta1_Pt25_Minv"] )         -> Fill(event.GetVecBosonP4().Rapidity(), mcEvent->mP4ZBoson.Rapidity());
        ((TH1*) o["hZBosonRap_Eta1_Pt25_Minv"] )                   -> Fill(mcEvent->mP4ZBoson.Rapidity());
        ((TH2*) o["hRecoVsGen_ZBosonPt_Eta1_Pt25_Minv"] )          -> Fill(event.GetVecBosonP4().Pt(), mcEvent->mP4ZBoson.Pt());
        ((TH2*) o["hRecoVsGen_ZBosonPt_Eta1_Pt25_Minv_XSecBins"] ) -> Fill(event.GetVecBosonP4().Pt(), mcEvent->mP4ZBoson.Pt());
        ((TH1*) o["hZBosonPt_Eta1_Pt25_Minv"] )                    -> Fill(mcEvent->mP4ZBoson.Pt());
        ((TH1*) o["hZBosonPt_Eta1_Pt25_Minv_XSecBins"] )           -> Fill(mcEvent->mP4ZBoson.Pt());
     }
   }

   ((TH2*) o["hRecoVsGen_ElectronE"] )  -> Fill(event.GetElectronCand_P4().E(), mcEvent->mP4Electron.E());
   ((TH2*) o["hRecoVsGen_ElectronPz"] ) -> Fill(event.GetElectronCand_P4().Pz(), mcEvent->mP4Electron.Pz());
   ((TH2*) o["hRecoVsGen_ElectronPt"] ) -> Fill(event.GetElectronCand_P4().Pt(), mcEvent->mP4Electron.Pt());

   ((TH2*) o["hRecoVsGen_PositronE"] )  -> Fill(event.GetPositronCand_P4().E(), mcEvent->mP4Positron.E());
   ((TH2*) o["hRecoVsGen_PositronPz"] ) -> Fill(event.GetPositronCand_P4().Pz(), mcEvent->mP4Positron.Pz());
   ((TH2*) o["hRecoVsGen_PositronPt"] ) -> Fill(event.GetPositronCand_P4().Pt(), mcEvent->mP4Positron.Pt());

   ((TH2*) o["hGen_ElectronVsPositron_Pt"] ) -> Fill(mcEvent->mP4Positron.Pt(), mcEvent->mP4Electron.Pt());
   ((TH2*) o["hGen_ElectronVsZBoson_Pt"] )   -> Fill(mcEvent->mP4ZBoson.Pt(), mcEvent->mP4Electron.Pt());
   ((TH2*) o["hGen_ElectronEtaVsZBosonRap"] )-> Fill(mcEvent->mP4ZBoson.Rapidity(), mcEvent->mP4Electron.Eta());


   ((TH2*) o["hTrackRecoilTpcNeutralsPt_GenOverReco"])       ->Fill(event.GetTrackRecoilTpcNeutrals().Pt(), mcEvent->mP4ZBoson.Pt()/event.GetTrackRecoilTpcNeutrals().Pt());
   ((TH2*) o["hTrackRecoilTpcNeutralsPt_GenOverReco_zoomin"])->Fill(event.GetTrackRecoilTpcNeutrals().Pt(), mcEvent->mP4ZBoson.Pt()/event.GetTrackRecoilTpcNeutrals().Pt());

}
