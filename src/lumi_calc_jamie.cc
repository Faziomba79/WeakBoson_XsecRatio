#include <string>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>

using namespace std;


int main(int argc, char *argv[])
{
   float lumi;
   float lumiSum = 0;
   int   runNo;
   int Counter_missingRuns =0;

   ifstream infile_runlist;
   infile_runlist.open("../runlists/run11_pp_transverse_SL17g");
   //infile_runlist.open("../runlists/run12_pp_long_j3");

   ifstream infile_lumitable;
   infile_lumitable.open("../runlists/lumitables_jamie/lumi_pp500_r11_BHT3L2BW.txt");

   while (!infile_runlist.eof() ) {
     infile_runlist >> runNo;
     cout << "runNo = " << runNo << endl;

     while (!infile_lumitable.eof() ) {
         int   runNo_jamie;
         float starttime;
         float endtime;
         float fillNo;
         float prescale;
	 //infile_runlist >> runNo;
	 //cout << "runNo = " << runNo << endl;

	 infile_lumitable >> runNo_jamie;
	 infile_lumitable >> starttime;
	 infile_lumitable >> endtime;
	 infile_lumitable >> fillNo;
	 infile_lumitable >> lumi;
	 infile_lumitable >> prescale;
	 
	 cout << "runNo_jamie = " << runNo_jamie << endl;
	 /*
	 cout << "starttime = " << starttime << endl;
	 cout << "endtime = " << endtime << endl;
	 cout << "fillNo = " << fillNo << endl;
	 cout << "lumi = " << lumi << endl;
	 cout << "prescale = " << prescale << endl;
	 */

	 if (runNo == runNo_jamie) {
	   lumiSum += lumi; 
	   cout << "Luminosity (pb-1): " << lumiSum << endl;
	   break;
	 }

	 if (runNo < runNo_jamie) {
	   Counter_missingRuns++;
	   break; //some runs are missing in Jamies files
	 }

     }

   }

   cout << "# missing runs = " << Counter_missingRuns << endl;

   return EXIT_SUCCESS;
}
